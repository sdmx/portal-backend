package org.soenda.portal.service.auth;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenManager {
  Environment env;
  byte[] secretKey;

  @Autowired
  public TokenManager(Environment env) {
    this.env = env;
    secretKey = DatatypeConverter.parseBase64Binary(env.getProperty("app.key"));
  }

  public String generateToken(Integer userId, int expTime) {
    Map<String, Object> claims = new HashMap<>();
    DateTime now = new DateTime(DateTimeZone.UTC);

    claims.put("sub", userId);
    claims.put("exp", now.plusSeconds(expTime).getMillis());

    SignatureAlgorithm algorithm = SignatureAlgorithm.HS256;
    Key key = new SecretKeySpec(secretKey, algorithm.getJcaName());

    return Jwts.builder().setClaims(claims).signWith(key, algorithm).compact();
  }

  public Jws<Claims> parse(String token) throws JwtException {
    JwtParser parser = Jwts.parserBuilder().setSigningKey(secretKey).build();
    return parser.parseClaimsJws(token);
  }
}
