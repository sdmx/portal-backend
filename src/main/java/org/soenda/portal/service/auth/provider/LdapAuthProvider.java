package org.soenda.portal.service.auth.provider;

import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import javax.naming.Name;
import org.soenda.portal.data.db.Permission;
import org.soenda.portal.data.db.User;
import org.soenda.portal.data.db.mapper.PermissionMapper;
import org.soenda.portal.data.db.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.support.LdapNameBuilder;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

@Service
public class LdapAuthProvider implements AuthenticationProvider {
  private Environment env;
  private LdapTemplate ldapTemplate;
  private UserMapper userMapper;
  private PermissionMapper permissionMapper;

  @Autowired
  public LdapAuthProvider(
      Environment env,
      LdapTemplate ldapTemplate,
      UserMapper userMapper,
      PermissionMapper permissionMapper) {
    this.env = env;
    this.ldapTemplate = ldapTemplate;
    this.userMapper = userMapper;
    this.permissionMapper = permissionMapper;
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return authentication.equals(UsernamePasswordAuthenticationToken.class);
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    String username = authentication.getName();
    String password = authentication.getCredentials().toString();
    String userBaseDn = env.getProperty("ldap.user.baseDn");
    String filterDn = MessageFormat.format(env.getProperty("ldap.user.filterDn"), username);

    try {
      if (ldapTemplate.authenticate(userBaseDn, filterDn, password)) {
        Name name = LdapNameBuilder.newInstance(userBaseDn).add(filterDn).build();
        User userLogin = ldapTemplate.findByDn(name, User.class);

        // Synchronize LDAP user into database
        Optional<User> user = userMapper.findByUsername(userLogin.getUsername());

        // Create new user if user not exists on DB
        if (!user.isPresent()) {
          userMapper.insertAuth(userLogin.getUsername(), userLogin.getName(), userLogin.getEmail());
          user = userMapper.findByUsername(userLogin.getUsername());
        }

        // Get user authorities
        Set<GrantedAuthority> authorities = new HashSet<>();

        // disable permissions
        if (Objects.equals(env.getProperty("permission.enable"), "false")) {
          for (Permission permission : permissionMapper.all()) {
            authorities.add(new SimpleGrantedAuthority(permission.getName()));
          }
        }
        // fetch user authorities
        else if (user.isPresent()) {
          authorities = getUserAuthoritiesById(user.get().getId());
        }

        return new UsernamePasswordAuthenticationToken(user.get(), password, authorities);
      }
    } catch (AuthenticationException e) {
      e.printStackTrace();
    }

    return null;
  }

  private Set<GrantedAuthority> getUserAuthoritiesById(Integer userId) {
    Set<GrantedAuthority> authorities = new HashSet<>();
    Map<String, Set<Permission>> permissions = permissionMapper.getByUserId(userId);

    for (Map.Entry<String, Set<Permission>> item : permissions.entrySet()) {
      authorities.add(new SimpleGrantedAuthority(item.getKey()));

      for (Permission permission : item.getValue()) {
        authorities.add(new SimpleGrantedAuthority(permission.getName()));
      }
    }

    return authorities;
  }
}
