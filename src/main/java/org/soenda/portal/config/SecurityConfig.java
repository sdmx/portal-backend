package org.soenda.portal.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import javax.annotation.PostConstruct;
import org.soenda.portal.data.db.Permission;
import org.soenda.portal.data.db.User;
import org.soenda.portal.data.db.mapper.PermissionMapper;
import org.soenda.portal.data.db.mapper.UserMapper;
import org.soenda.portal.service.auth.provider.LdapAuthProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@Configuration
// @EnableWebSecurity
// @EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
// class SecurityConfig extends WebSecurityConfigurerAdapter {
class SecurityConfig {
  @Autowired private Environment env;
  @Autowired private UserMapper userMapper;
  @Autowired private PermissionMapper permissionMapper;
  @Autowired private DataSourceTransactionManager transactionManager;

  // @PostConstruct
  // public void initialize() {
  //   // initialize permission
  //   if (Objects.equals(env.getProperty("permission.initialize"), "true")) {
  //     TransactionStatus txStatus =
  //         transactionManager.getTransaction(new DefaultTransactionDefinition());

  //     try (InputStream input = new FileInputStream("permission.properties")) {
  //       Properties props = new Properties();
  //       props.load(input);
  //       permissionMapper.clear();

  //       for (Map.Entry<Object, Object> item : props.entrySet()) {
  //         Permission data = new Permission(item.getKey().toString(), item.getValue().toString());
  //         permissionMapper.insert(data);
  //       }

  //       transactionManager.commit(txStatus);
  //     } catch (IOException e) {
  //       e.printStackTrace();
  //     } catch (Exception e) {
  //       transactionManager.rollback(txStatus);
  //       e.printStackTrace();
  //     }
  //   }
  // }

  @Bean
  public TokenBasedRememberMeServices rememberMeServices() {
    return new TokenBasedRememberMeServices(
        "remember-me-key",
        new UserDetailsService() {
          @Override
          public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
            Optional<User> user = userMapper.findByUsername(username);

            if (!user.isPresent()) {
              throw new UsernameNotFoundException("User '" + username + "' not found.");
            }

            return new org.springframework.security.core.userdetails.User(
                username, null, new HashSet<>());
          }
        });
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  // @Override
  // @Order(1)
  // protected void configure(AuthenticationManagerBuilder auth) throws Exception {
  //   // LDAP Auhentication Provider
  //   auth.authenticationProvider(
  //       new LdapAuthProvider(env, ldapTemplate(), userMapper, permissionMapper));
  // }

  // @Override
  // protected void configure(HttpSecurity http) throws Exception {
  //   http.csrf()
  //       .disable()
  //       .authorizeRequests()
  //       .antMatchers("/**")
  //       .permitAll()
  //       .anyRequest()
  //       .authenticated()
  //       .and()
  //       .formLogin()
  //       .loginPage("/login")
  //       .permitAll()
  //       .failureUrl("/login?error")
  //       .loginProcessingUrl("/authenticate")
  //       .and()
  //       .logout()
  //       .logoutUrl("/logout")
  //       .permitAll()
  //       .logoutSuccessUrl("/login?logout")
  //       .and()
  //       .rememberMe()
  //       .rememberMeServices(rememberMeServices())
  //       .key("remember-me-key");
  // }

  @Bean
  public LdapTemplate ldapTemplate() throws Exception {
    LdapContextSource ctx = new LdapContextSource();
    ctx.setUrl(env.getProperty("ldap.url"));
    ctx.setBase(env.getProperty("ldap.baseDn"));
    ctx.setUserDn(env.getProperty("ldap.bindDn"));
    ctx.setPassword(env.getProperty("ldap.bindPassword"));
    ctx.afterPropertiesSet();

    LdapTemplate template = new LdapTemplate(ctx);
    template.afterPropertiesSet();

    return template;
  }

  @Bean
  @Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
  public User user(Authentication auth) {
    if (auth != null) {
      return (User) auth.getPrincipal();
    }

    return null;
  }
}
