package org.soenda.portal.config;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
public class AppConfig {
  public static final Path storagePath = Paths.get(System.getProperty("user.home"), ".portal");
  Environment env;

  @Autowired
  public AppConfig(Environment env) {
    this.env = env;
  }

  @Bean
  public JavaMailSender getJavaMailSender() {
    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    mailSender.setHost(env.getProperty("mail.host"));
    mailSender.setPort(Integer.parseInt(env.getProperty("mail.port")));
    mailSender.setUsername(env.getProperty("mail.username"));
    mailSender.setPassword(env.getProperty("mail.password"));

    Properties props = mailSender.getJavaMailProperties();
    props.put("mail.transport.protocol", "smtp");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.starttls.enable", "true");
    props.put("mail.debug", env.getProperty("mail.debug"));
    props.put("mail.from.email", env.getProperty("mail.from"));
    mailSender.setJavaMailProperties(props);

    return mailSender;
  }

  /* @Bean
  public LocaleResolver localeResolver() {
    CookieLocaleResolver localeResolver = new CookieLocaleResolver();
    localeResolver.setCookieName("localeInfo");
    localeResolver.setCookieMaxAge(24 * 60 * 60);
    localeResolver.setDefaultLocale(Locale.ENGLISH);

    return localeResolver;
  }

  @Bean
  public ReloadableResourceBundleMessageSource messageSource() {
    ReloadableResourceBundleMessageSource messageSource =
        new ReloadableResourceBundleMessageSource();
    messageSource.setCacheSeconds(5);
    messageSource.setDefaultEncoding("UTF-8");
    messageSource.setBasenames("classpath:locale/app", "classpath:locale/message",
        "classpath:locale/validation");

    return messageSource;
  } */
}
