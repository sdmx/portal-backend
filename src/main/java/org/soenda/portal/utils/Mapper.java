package org.soenda.portal.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Mapper {
  static ObjectMapper instance;

  private Mapper() {}

  public static ObjectMapper get() {
    if (instance == null) {
      instance = new ObjectMapper();
    }

    return instance;
  }
}
