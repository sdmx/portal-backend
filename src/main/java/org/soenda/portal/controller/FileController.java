package org.soenda.portal.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.soenda.portal.config.AppConfig;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.HandlerMapping;

@Controller
class FileController {
  int BYTES_DOWNLOAD = 1024 * 4;

  @ResponseBody
  @RequestMapping(value = "file/**", method = RequestMethod.GET)
  public void file(HttpServletRequest req, HttpServletResponse res)
      throws FileNotFoundException, IOException {
    Path file = Paths
        .get(req.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE).toString());
    Path filepath = AppConfig.storagePath.resolve(file.subpath(1, file.getNameCount()));
    File downloadFile = filepath.toFile();

    // File not found
    if (!downloadFile.exists() || downloadFile.isDirectory()) {
      res.setStatus(404);
      return;
    }

    if (req.getParameter("download") != null) {
      res.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
      res.setHeader("Content-Disposition",
          String.format("attachment; filename=\"%s\"", filepath.getFileName()));
    } else {
      FileNameMap fileNameMap = URLConnection.getFileNameMap();
      res.setContentType(fileNameMap.getContentTypeFor(filepath.getFileName().toString()));
      res.setHeader("Content-Disposition", "inline");
    }

    InputStream is = new FileInputStream(downloadFile);
    OutputStream os = res.getOutputStream();
    int read = 0;
    byte[] bytes = new byte[BYTES_DOWNLOAD];

    while ((read = is.read(bytes)) != -1) {
      os.write(bytes, 0, read);
    }

    os.flush();
    os.close();
    is.close();
  }
}
