package org.soenda.portal.controller;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import com.google.common.base.Throwables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class AppErrorController implements ErrorController {
  @Autowired private Environment env;

  @Override
  public String getErrorPath() {
    return "/error";
  }

  @RequestMapping(value = "/error")
  public Map<String, Object> error(HttpServletRequest req) {
    Integer statusCode = (Integer) req.getAttribute("javax.servlet.error.status_code");

    if (statusCode == null) {
      statusCode = 500;
    }

    String reason = HttpStatus.valueOf(statusCode).getReasonPhrase();
    Throwable throwable = (Throwable) req.getAttribute("javax.servlet.error.exception");

    Map<String, Object> response = new HashMap<>();
    response.put("title", reason);
    response.put("statusCode", statusCode);

    if (throwable != null) {
      String exceptionMessage = Throwables.getRootCause(throwable).getMessage();
      response.put("message", exceptionMessage);
    }

    return response;
  }
}
