package org.soenda.portal.api;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.joda.time.DateTime;
import org.soenda.portal.data.db.User;
import org.soenda.portal.service.auth.TokenManager;
import org.soenda.portal.service.auth.provider.LdapAuthProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import io.jsonwebtoken.Claims;

@RestController
@RequestMapping("auth")
class AuthController {
  final int expiredTime = 15 * 60;
  final String cookieName = "u";

  LdapAuthProvider ldapAuthProvider;
  TokenManager tokenManager;

  @PostMapping("refresh")
  ResponseEntity<Map<String, Object>> refresh(final HttpServletRequest request,
      final HttpServletResponse response) {
    final Cookie[] cookies = request.getCookies();
    String token = null;

    if (cookies != null) {
      for (final Cookie cookie : cookies) {
        if (cookie.getName().equals(cookieName)) {
          token = cookie.getValue();
        }
      }
    }

    // empty token
    if (token == null) {
      return getFailResponse("Bad Request", HttpStatus.BAD_REQUEST);
    }

    try {
      final Claims claims = tokenManager.parse(token).getBody();
      final DateTime exp = new DateTime(claims.get("exp"));

      // token is expired
      if (exp.isBeforeNow()) {
        return getFailResponse("Token is expired!", HttpStatus.FORBIDDEN);
      }

      return getResponse(response, (Integer) claims.get("sub"));
    } catch (Exception e) {
      return getFailResponse("Token is invalid!", HttpStatus.BAD_REQUEST);
    }
  }

  @PostMapping("login")
  ResponseEntity<Map<String, Object>> login(final HttpServletResponse response,
      @RequestParam("username") final String username,
      @RequestParam("password") final String password) {
    final Authentication loginData = new UsernamePasswordAuthenticationToken(username, password);
    final Authentication auth = ldapAuthProvider.authenticate(loginData);

    // login failed
    if (auth == null) {
      return getFailResponse("User not found!", HttpStatus.FORBIDDEN);
    }

    final User user = (User) auth.getPrincipal();

    return getResponse(response, user.getId());
  }

  @PostMapping("logout")
  ResponseEntity<String> logout(final HttpServletRequest request,
      final HttpServletResponse response) {
    final Cookie cookie = new Cookie(cookieName, null);
    cookie.setMaxAge(0);
    cookie.setHttpOnly(true);
    cookie.setPath("/");
    response.addCookie(cookie);

    return new ResponseEntity<>(null, HttpStatus.OK);
  }

  ResponseEntity<Map<String, Object>> getResponse(final HttpServletResponse response,
      final Integer userId) {
    final Map<String, Object> body = new HashMap<>();
    body.put("accessToken", tokenManager.generateToken(userId, expiredTime));
    body.put("tokenExpiry", expiredTime);

    // set httpOnly Cookie for refresh token
    final String refreshToken = tokenManager.generateToken(userId, 60 * 24 * 7);
    final Cookie cookie = new Cookie(cookieName, refreshToken);
    cookie.setMaxAge(7 * 24 * 60 * 60);
    cookie.setHttpOnly(true);
    cookie.setPath("/");
    response.addCookie(cookie);

    return new ResponseEntity<>(body, HttpStatus.OK);
  }

  ResponseEntity<Map<String, Object>> getFailResponse(final String message,
      final HttpStatus status) {
    final Map<String, Object> body = new HashMap<>();
    body.put("message", message);

    return new ResponseEntity<>(body, status);
  }

  @Autowired
  void setLdapAuthProvider(final LdapAuthProvider ldapAuthProvider) {
    this.ldapAuthProvider = ldapAuthProvider;
  }

  @Autowired
  void setTokenManager(final TokenManager tokenManager) {
    this.tokenManager = tokenManager;
  }
}
