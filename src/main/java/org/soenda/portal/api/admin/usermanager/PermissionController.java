package org.soenda.portal.api.admin.usermanager;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("admin/permission")
public class PermissionController {

  // @GetMapping
  // public String config(Model model) {
  // 	List<Role> roles = roleRepository.findAll();
  // 	List<Permission> permissions = permissionRepository.findAll();
  // 	Map<String, List<Permission>> data = new LinkedHashMap<>();

  // 	for (Permission permission : permissions) {
  // 		String permission_name = permission.getName();
  // 		String feature = permission_name.substring(0, permission_name.indexOf("."));

  // 		data.putIfAbsent(feature, new ArrayList<>());
  // 		data.get(feature).add(permission);
  // 	}

  // 	model.addAttribute("roles", roles);
  // 	model.addAttribute("permissions", data);

  // 	return "admin/permission/config";
  // }

  // @PostMapping
  // public String store(HttpServletRequest req) {
  // 	EntityManager em = modelEntityFactory.em();
  // 	em.getTransaction().begin();
  // 	Enumeration<String> e = req.getParameterNames();

  // 	while (e.hasMoreElements()) {
  // 		String param = e.nextElement();
  // 		Matcher matcher = Pattern.compile("^config_(\\d+)").matcher(param);

  // 		if (matcher.find()) {
  // 			Role role = em.getReference(Role.class, new Long(matcher.group(1)));
  // 			role.emptyPermission();

  // 			for (String perm_id : req.getParameterValues(param)) {
  // 				role.addPermission(em.getReference(Permission.class, new Long(perm_id)));
  // 			}

  // 			em.persist(role);
  // 		}
  // 	}

  // 	em.getTransaction().commit();
  // 	em.close();

  // 	return "redirect:/admin/permission";
  // }
}
