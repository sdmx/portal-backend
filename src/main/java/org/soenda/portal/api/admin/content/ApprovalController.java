package org.soenda.portal.api.admin.content;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("admin/approval")
class ApprovalController {

  // @GetMapping({"", "/", "news"})
  // @PreAuthorize("hasAuthority('approval.news')")
  // public String listNews(Principal principal, Model model) {
  //   model.addAttribute("type", "news");
  //   model.addAttribute("data", newsApprovalService.scopeAuth(principal).paginate(pageable));

  //   return "admin/approval/list";
  // }

  // @PostMapping("news/approve/{id}")
  // @PreAuthorize("hasAuthority('approval.news')")
  // public String approveNews(@PathVariable("id") long id,
  //     @RequestParam(value = "note", required = false) String note) {
  //   NewsApproval approval = newsApprovalService.findOrFail(id);
  //   newsApprovalService.approve(approval, note);

  //   return "redirect:/admin/approval";
  // }

  // @PostMapping("news/deny/{id}")
  // @PreAuthorize("hasAuthority('approval.news')")
  // public String denyNews(@PathVariable("id") long id,
  //     @RequestParam(value = "note", required = false) String note) {
  //   NewsApproval approval = newsApprovalService.findOrFail(id);
  //   newsApprovalService.reject(approval, note);

  //   return "redirect:/admin/approval/news";
  // }

  // /** PUBLICATION */

  // @GetMapping("publication")
  // @PreAuthorize("hasAuthority('approval.publication')")
  // public String listPublication(Principal principal, Model model) {
  //   model.addAttribute("type", "publication");
  //   model.addAttribute("data",
  // publicationApprovalService.scopeAuth(principal).paginate(pageable));

  //   return "admin/approval/list";
  // }

  // @PostMapping("publication/approve/{id}")
  // @PreAuthorize("hasAuthority('approval.publication')")
  // public String approvePublication(@PathVariable("id") long id,
  //     @RequestParam(value = "note", required = false) String note) {
  //   PublicationApproval approval = publicationApprovalService.findOrFail(id);
  //   publicationApprovalService.approve(approval, note);

  //   return "redirect:/admin/approval/publication";
  // }

  // @PostMapping("publication/deny/{id}")
  // @PreAuthorize("hasAuthority('approval.publication')")
  // public String denyPublication(@PathVariable("id") long id,
  //     @RequestParam(value = "note", required = false) String note) {
  //   PublicationApproval approval = publicationApprovalService.findOrFail(id);
  //   publicationApprovalService.reject(approval, note);

  //   return "redirect:/admin/approval/publication";
  // }
}
