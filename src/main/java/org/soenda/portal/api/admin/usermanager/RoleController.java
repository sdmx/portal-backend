package org.soenda.portal.api.admin.usermanager;

import java.util.Map;
import java.util.Optional;
import org.soenda.portal.data.db.Role;
import org.soenda.portal.data.db.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping("admin/role")
// @PreAuthorize("hasAuthority('role.view')")
public class RoleController {
  // @Autowired
  // private Pageable pageable;
  // @Autowired
  // private RoleMapper roleMapper;

  // @GetMapping
  // public String list(Model model, @RequestParam Map<String, String> param) {
  //   model.addAttribute("data", roleMapper.paginate(pageable, param.getOrDefault("q", "")));

  //   return "admin/role/list";
  // }

  // @GetMapping("create")
  // @PreAuthorize("hasAuthority('role.create')")
  // public String create(Model model) {
  //   model.addAttribute("title", "Create New Role");
  //   model.addAttribute("model", new Role());

  //   return "admin/role/form";
  // }

  // @PostMapping("create")
  // @PreAuthorize("hasAuthority('role.create')")
  // public String store(@RequestParam Map<String, String> param) {
  //   roleMapper.insert(new Role(param.get("name"), param.get("description")));

  //   return "redirect:/admin/role";
  // }

  // @GetMapping("edit/{id}")
  // @PreAuthorize("hasAuthority('role.update')")
  // public String edit(Model model, @PathVariable("id") int id) throws ResponseStatusException {
  //   Optional<Role> role = roleMapper.findById(id);

  //   if (!role.isPresent()) {
  //     throw new ResponseStatusException(HttpStatus.NOT_FOUND);
  //   }

  //   role.ifPresent((data) -> {
  //     model.addAttribute("title", "Edit Role");
  //     model.addAttribute("model", data);
  //   });

  //   return "admin/role/form";
  // }

  // @PostMapping("edit/{id}")
  // @PreAuthorize("hasAuthority('role.update')")
  // public String update(@RequestParam Map<String, String> param, @PathVariable("id") int id) {
  //   Optional<Role> role = roleMapper.findById(id);

  //   if (!role.isPresent()) {
  //     throw new ResponseStatusException(HttpStatus.NOT_FOUND);
  //   }

  //   role.ifPresent((data) -> {
  //     data.setName(param.get("name"));
  //     data.setDescription(param.get("description"));
  //     roleMapper.update(id, data);
  //   });

  //   return "redirect:/admin/role";
  // }

  // @PostMapping("delete/{id}")
  // @PreAuthorize("hasAuthority('role.delete')")
  // public String delete(@PathVariable("id") int id) {
  //   Optional<Role> role = roleMapper.findById(id);

  //   if (!role.isPresent()) {
  //     throw new ResponseStatusException(HttpStatus.NOT_FOUND);
  //   }

  //   role.ifPresent((data) -> {
  //     roleMapper.delete(id);
  //   });

  //   return "redirect:/admin/role";
  // }
}
