package org.soenda.portal.data.db;

import javax.naming.Name;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;
import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;
import org.springframework.ldap.odm.annotations.Transient;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@Entry(objectClasses = {"inetOrgPerson", "person", "top"})
public class User implements java.io.Serializable {
  private static final long serialVersionUID = 1L;

  private Integer id;

  @Id
  private Name dn;

  @Attribute(name = "uid")
  private String username;

  @Attribute(name = "cn")
  private String name;

  @Attribute(name = "mail")
  private String email;

  private Integer organisationId;

  private Integer positionId;

  private char status;

  @Transient
  private DateTime deleted;
}
