package org.soenda.portal.data.db.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

@MappedTypes(Boolean.class)
public class BooleanHandler implements TypeHandler<Boolean> {

  @Override
  public void setParameter(PreparedStatement ps, int i, Boolean value, JdbcType jdbcType)
      throws SQLException {
    ps.setString(i, value ? "1" : "0");
  }

  @Override
  public Boolean getResult(ResultSet rs, String columnName) throws SQLException {
    return parse(rs.getString(columnName));
  }

  @Override
  public Boolean getResult(ResultSet rs, int columnIndex) throws SQLException {
    return parse(rs.getString(columnIndex));
  }

  @Override
  public Boolean getResult(CallableStatement cs, int columnIndex) throws SQLException {
    return parse(cs.getString(columnIndex));
  }

  private Boolean parse(String value) {
    return Objects.equals(value, "1");
  }
}
