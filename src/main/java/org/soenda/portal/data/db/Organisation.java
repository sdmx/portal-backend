package org.soenda.portal.data.db;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Organisation implements java.io.Serializable {
  private static final long serialVersionUID = 1L;

  private Integer id;
  private String name;
  private Integer parentId;
}
