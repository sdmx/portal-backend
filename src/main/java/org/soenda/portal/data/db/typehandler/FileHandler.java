package org.soenda.portal.data.db.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;
import org.soenda.portal.data.FileData;

@MappedTypes({FileData.class})
public class FileHandler implements TypeHandler<FileData> {

  @Override
  public void setParameter(PreparedStatement ps, int i, FileData value, JdbcType jdbcType)
      throws SQLException {
    ps.setString(i, value == null ? null : value.getPath());
  }

  @Override
  public FileData getResult(ResultSet rs, String columnName) throws SQLException {
    return parse(rs.getString(columnName));
  }

  @Override
  public FileData getResult(ResultSet rs, int columnIndex) throws SQLException {
    return parse(rs.getString(columnIndex));
  }

  @Override
  public FileData getResult(CallableStatement cs, int columnIndex) throws SQLException {
    return parse(cs.getString(columnIndex));
  }

  private FileData parse(String value) {
    return value == null ? null : new FileData(value);
  }
}
