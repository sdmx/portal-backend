package org.soenda.portal.data.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;
import org.soenda.portal.data.FileData;
import org.soenda.portal.data.db.enums.Privilege;
import org.soenda.portal.data.db.enums.Status;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode
public class News implements java.io.Serializable {
  private static final long serialVersionUID = 1L;

  Long id;
  String name;
  String body;
  List<String> links;
  Map<String, String> attributes;
  FileData thumbnail;
  char status = Status.INACTIVE.getValue();
  char privilege = Privilege.PUBLIC.getValue();
  Boolean isPublished = false;
  DateTime created;
  DateTime updated;

  Integer userId;
  List<Category> categories = new ArrayList<>();

  public void addCategory(Category category) {
    categories.add(category);
  }

  public void setCategoryIds(List<Integer> categories) {
    for (Integer categoryId : categories) {
      Category category = new Category();
      category.setId(categoryId);
      addCategory(category);
    }
  }
}
