package org.soenda.portal.data.db.enums;

import lombok.Getter;

public enum ApprovalStatus {
  DRAFT('0'), APPROVED('1'), REJECTED('0'), PROCESSED('1');

  @Getter
  private final char value;

  private ApprovalStatus(char value) {
    this.value = value;
  }

  public static ApprovalStatus get(char value) {
    for (ApprovalStatus status : ApprovalStatus.values()) {
      if (status.getValue() == value) {
        return status;
      }
    }

    // default status
    return DRAFT;
  }
}
