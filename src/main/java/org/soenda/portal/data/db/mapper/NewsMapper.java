package org.soenda.portal.data.db.mapper;

import java.util.List;
import java.util.Optional;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.soenda.portal.data.db.Category;
import org.soenda.portal.data.db.News;
import org.soenda.portal.data.db.User;
import org.soenda.portal.data.db.sqlbuilder.NewsSqlBuilder;
import org.springframework.data.domain.Sort;

public interface NewsMapper extends IConnectionMapper<News, Long> {
  int insert(@Param("data") News data);

  int update(@Param("id") Long id, @Param("data") News data);

  int delete(@Param("id") Long id);

  Boolean exists(@Param("id") Long id);

  int addCategory(@Param("newsId") Long newsId, @Param("categoryId") Integer categoryId);

  int deleteAllCategories(@Param("newsId") Long newsId);

  List<Category> getCategoriesById(@Param("id") Long id);

  Optional<User> getUserById(@Param("id") Long id);

  @Override
  @SelectProvider(type = NewsSqlBuilder.class, method = "all")
  List<News> all(@Param("filter") String filter, @Param("limit") Integer limit,
      @Param("offset") Integer offset, @Param("sort") Sort sort, @Param("afterId") Integer afterId);

  @SelectProvider(type = NewsSqlBuilder.class, method = "all")
  List<News> findAll(@Param("filter") String filter, @Param("limit") Integer limit,
      @Param("offset") Integer offset, @Param("sort") Sort sort, @Param("afterId") Integer afterId,
      @Param("userId") Integer userId, @Param("isPublished") Boolean isPublished,
      @Param("status") Character status, @Param("privilege") Character privilege);

  @Override
  List<News> findByIds(@Param("ids") List<Long> ids);

  @Override
  Optional<News> findById(@Param("id") Long id);

  @Override
  Long lastInsertId();

  @Override
  @SelectProvider(type = NewsSqlBuilder.class, method = "count")
  Long count(@Param("filter") String filter);

  @SelectProvider(type = NewsSqlBuilder.class, method = "count")
  Long filterCount(@Param("filter") String filter, @Param("userId") Integer userId,
      @Param("isPublished") Boolean isPublished, @Param("status") Character status,
      @Param("privilege") Character privilege);
}
