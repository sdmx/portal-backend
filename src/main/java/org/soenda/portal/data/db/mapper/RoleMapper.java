package org.soenda.portal.data.db.mapper;

import java.util.List;
import java.util.Optional;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.soenda.portal.data.db.Role;
import org.soenda.portal.data.db.sqlbuilder.RoleSqlBuilder;
import org.springframework.data.domain.Pageable;

public interface RoleMapper {
  /** SELECT */
  @Select("select * from roles where id=#{id}")
  Optional<Role> findById(@Param("id") int id);

  @SelectProvider(type = RoleSqlBuilder.class, method = "paginate")
  List<Role> paginate(@Param("pageable") Pageable pageable, @Param("query") String query);

  @Select("select r.* from users u inner join users_roles ur on ur.user_id=u.id"
      + " inner join roles r on ur.role_id=r.id where u.id=#{userId}")
  List<Role> getByUserId(@Param("userId") int userId);

  /** INSERT */
  @Insert("insert into roles (name, description) values (#{data.name}, #{data.description})")
  int insert(@Param("data") Role data);

  /** UPDATE */
  @Update("update roles set name=#{data.name}, description=#{data.description} where id=#{id}")
  int update(@Param("id") int id, @Param("data") Role data);

  /** DELETE */
  @Delete("delete from roles where id=#{id}")
  int delete(@Param("id") int id);
}
