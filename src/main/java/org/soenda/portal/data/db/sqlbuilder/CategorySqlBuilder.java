package org.soenda.portal.data.db.sqlbuilder;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.soenda.portal.data.db.sqlbuilder.ConnectionSqlBuilder.TableDefinition;
import org.springframework.data.domain.Sort;

public class CategorySqlBuilder {
  public static TableDefinition tableDef =
      new TableDefinition("id", "categories", new String[] {"name"});

  public static String all(@Param("filter") String filter, @Param("limit") Integer limit,
      @Param("offset") Integer offset, @Param("sort") Sort sort,
      @Param("afterId") Integer afterId) {
    return ConnectionSqlBuilder.all(tableDef, filter, limit, offset, sort, afterId);
  }

  public static String count(@Param("filter") String filter) {
    return ConnectionSqlBuilder.count(tableDef, filter);
  }

  public static String findByIds(@Param("ids") final List<Integer> ids) {
    return ConnectionSqlBuilder.findByIds(tableDef, ids);
  }
}
