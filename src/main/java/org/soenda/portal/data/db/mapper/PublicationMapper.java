package org.soenda.portal.data.db.mapper;

import java.util.List;
import java.util.Optional;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.soenda.portal.data.db.Category;
import org.soenda.portal.data.db.Publication;
import org.soenda.portal.data.db.User;
import org.soenda.portal.data.db.sqlbuilder.PublicationSqlBuilder;
import org.springframework.data.domain.Sort;

public interface PublicationMapper extends IConnectionMapper<Publication, Long> {
  int insert(@Param("data") Publication data);

  int update(@Param("id") Long id, @Param("data") Publication data);

  int delete(@Param("id") Long id);

  Boolean exists(@Param("id") Long id);

  int addCategory(@Param("publicationId") Long publicationId,
      @Param("categoryId") Integer categoryId);

  int deleteAllCategories(@Param("publicationId") Long publicationId);

  List<Category> getCategoriesById(@Param("id") Long id);

  Optional<User> getUserById(@Param("id") Long id);

  @Override
  @SelectProvider(type = PublicationSqlBuilder.class, method = "all")
  List<Publication> all(@Param("filter") String filter, @Param("limit") Integer limit,
      @Param("offset") Integer offset, @Param("sort") Sort sort, @Param("afterId") Integer afterId);

  @SelectProvider(type = PublicationSqlBuilder.class, method = "all")
  List<Publication> findAll(@Param("filter") String filter, @Param("limit") Integer limit,
      @Param("offset") Integer offset, @Param("sort") Sort sort, @Param("afterId") Integer afterId,
      @Param("userId") Integer userId, @Param("isPublished") Boolean isPublished,
      @Param("status") Character status, @Param("privilege") Character privilege);

  @Override
  List<Publication> findByIds(@Param("ids") List<Long> ids);

  @Override
  Optional<Publication> findById(@Param("id") Long id);

  @Override
  Long lastInsertId();

  @Override
  @SelectProvider(type = PublicationSqlBuilder.class, method = "count")
  Long count(@Param("filter") String filter);

  @SelectProvider(type = PublicationSqlBuilder.class, method = "count")
  Long filterCount(@Param("filter") String filter, @Param("userId") Integer userId,
      @Param("isPublished") Boolean isPublished, @Param("status") Character status,
      @Param("privilege") Character privilege);
}
