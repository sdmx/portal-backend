package org.soenda.portal.data.db.mapper;

import java.util.List;
import java.util.Optional;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.soenda.portal.data.db.User;
import org.soenda.portal.data.db.sqlbuilder.UserSqlBuilder;
import org.soenda.portal.data.db.typehandler.DateTimeHandler;
import org.springframework.data.domain.Pageable;

public interface UserMapper {
  /** SELECT */
  @Results(id = "userResult", value = {
      @Result(column = "deleted", property = "deleted", typeHandler = DateTimeHandler.class)})
  @Select("select * from users where id=#{id}")
  Optional<User> findById(@Param("id") int id);

  @ResultMap("userResult")
  @SelectProvider(type = UserSqlBuilder.class, method = "paginate")
  List<User> paginate(@Param("pageable") Pageable pageable, @Param("query") String query);

  @ResultMap("userResult")
  @Select("select * from users where username=#{username}")
  Optional<User> findByUsername(@Param("username") String username);

  /** INSERT */
  @Insert("insert into users (username, name, email, organisation_id, position_id) "
      + "values (#{data.username}, #{data.name}, #{data.email}, #{data.organisationId}, "
      + "#{data.positionId})")
  int insert(@Param("data") User data);

  @Insert("insert into users (username, name, email) values (#{username}, #{name}, #{email})")
  int insertAuth(@Param("username") String username, @Param("name") String name,
      @Param("email") String email);

  /** UPDATE */
  @Update("update users set username=#{data.username}, email=#{data.email}, "
      + "name=#{data.name}, organisation_id=#{data.organisationId}, "
      + "position_id=#{data.positionId} status=#{data.status} where id=#{id}")
  int update(@Param("id") int id, @Param("data") User data);

  /** DELETE */
  @Delete("delete from users where id=#{id}")
  int delete(@Param("id") int id);
}
