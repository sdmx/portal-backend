package org.soenda.portal.data.db.sqlbuilder;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.data.domain.Pageable;

public class UserSqlBuilder {
  public static String paginate(
      @Param("pageable") final Pageable pageable, @Param("query") final String query) {
    return new SQL() {
      {
        SELECT("*");
        FROM("users");

        if (query != null && query.length() > 0) {
          WHERE("name like #{query}");
        }

        LIMIT(Long.valueOf(pageable.getPageSize()).intValue());
        OFFSET(Long.valueOf(pageable.getOffset()));
      }
    }.toString();
  }
}
