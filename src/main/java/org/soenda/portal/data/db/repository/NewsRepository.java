package org.soenda.portal.data.db.repository;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.apache.commons.io.FilenameUtils;
import org.joda.time.DateTime;
import org.soenda.portal.data.FileData;
import org.soenda.portal.data.db.Category;
import org.soenda.portal.data.db.News;
import org.soenda.portal.data.db.mapper.NewsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@Repository
public class NewsRepository {
  private DataSourceTransactionManager transactionManager;
  private NewsMapper newsMapper;

  @Autowired
  public NewsRepository(DataSourceTransactionManager transactionManager, NewsMapper newsMapper) {
    this.transactionManager = transactionManager;
    this.newsMapper = newsMapper;
  }

  public News create(News data) {
    TransactionStatus txStatus =
        transactionManager.getTransaction(new DefaultTransactionDefinition());

    try {
      newsMapper.insert(data);

      Long id = newsMapper.lastInsertId();
      data.setId(id);
      data.setCreated(DateTime.now());
      data.setUpdated(DateTime.now());

      addCategories(id, data.getCategories());
      uploadThumbnail(data.getThumbnail());

      transactionManager.commit(txStatus);

      return data;
    } catch (Exception e) {
      transactionManager.rollback(txStatus);
      e.printStackTrace();
    }

    return null;
  }

  public News update(Long id, News data) {
    Optional<News> news = newsMapper.findById(id);
    TransactionStatus txStatus =
        transactionManager.getTransaction(new DefaultTransactionDefinition());

    if (news.isPresent()) {
      try {
        newsMapper.update(id, data);
        data.setId(id);
        data.setUpdated(DateTime.now());

        // categories
        newsMapper.deleteAllCategories(id);
        addCategories(id, data.getCategories());

        // upload new thumbnail
        FileData thumbnail = data.getThumbnail();

        if (thumbnail != null) {
          uploadThumbnail(thumbnail);

          // delete old thumbnail
          FileData oldThumbnail = news.get().getThumbnail();

          if (oldThumbnail != null) {
            oldThumbnail.delete();
          }
        }

        transactionManager.commit(txStatus);

        return data;
      } catch (Exception e) {
        transactionManager.rollback(txStatus);
        e.printStackTrace();
      }
    }

    return null;
  }

  public News delete(Long id) {
    Optional<News> news = newsMapper.findById(id);
    TransactionStatus txStatus =
        transactionManager.getTransaction(new DefaultTransactionDefinition());

    if (news.isPresent()) {
      try {
        newsMapper.deleteAllCategories(id);
        newsMapper.delete(id);

        transactionManager.commit(txStatus);

        // delete thumbnail
        FileData thumbnail = news.get().getThumbnail();

        if (thumbnail != null) {
          thumbnail.delete();
        }

        return news.get();
      } catch (Exception e) {
        transactionManager.rollback(txStatus);
        e.printStackTrace();
      }
    }

    return null;
  }

  private void uploadThumbnail(FileData thumbnail) throws IOException {
    if (thumbnail != null) {
      String filename = String.format("news/%d-%s.%s", DateTime.now().getMillis(),
          UUID.randomUUID().toString(), FilenameUtils.getExtension(thumbnail.getFilename()));
      thumbnail.copyTo(filename);
    }
  }

  private void addCategories(Long id, List<Category> categories) {
    for (Category category : categories) {
      newsMapper.addCategory(id, category.getId());
    }
  }
}
