package org.soenda.portal.data.db.enums;

import lombok.Getter;

public enum Privilege {
  PUBLIC('1'), INTERNAL('0');

  @Getter
  private final char value;

  private Privilege(char value) {
    this.value = value;
  }

  public static Privilege get(char value) {
    for (Privilege status : Privilege.values()) {
      if (status.getValue() == value) {
        return status;
      }
    }

    // default value
    return PUBLIC;
  }
}
