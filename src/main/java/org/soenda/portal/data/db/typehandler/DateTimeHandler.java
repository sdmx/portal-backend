package org.soenda.portal.data.db.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.TimeZone;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;
import org.joda.time.DateTime;

@MappedTypes(DateTime.class)
public class DateTimeHandler implements TypeHandler<DateTime> {
  private static final Calendar UTC_CALENDAR = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

  @Override
  public void setParameter(PreparedStatement ps, int i, DateTime value, JdbcType jdbcType)
      throws SQLException {
    ps.setTimestamp(i, value == null ? null : new Timestamp(value.getMillis()), UTC_CALENDAR);
  }

  @Override
  public DateTime getResult(ResultSet rs, String columnName) throws SQLException {
    return parse(rs.getTimestamp(columnName, UTC_CALENDAR));
  }

  @Override
  public DateTime getResult(ResultSet rs, int columnIndex) throws SQLException {
    return parse(rs.getTimestamp(columnIndex, UTC_CALENDAR));
  }

  @Override
  public DateTime getResult(CallableStatement cs, int columnIndex) throws SQLException {
    return parse(cs.getTimestamp(columnIndex, UTC_CALENDAR));
  }

  private DateTime parse(Timestamp value) {
    return value != null ? new DateTime(value.getTime()) : null;
  }
}
