package org.soenda.portal.data.db.mapper;

import java.util.List;
import java.util.Optional;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.soenda.portal.data.db.Category;
import org.soenda.portal.data.db.sqlbuilder.CategorySqlBuilder;
import org.springframework.data.domain.Sort;

public interface CategoryMapper extends IConnectionMapper<Category, Integer> {
  int insert(@Param("data") Category data);

  int update(@Param("id") int id, @Param("data") Category data);

  int delete(@Param("id") int id);

  Boolean exists(@Param("id") int id);

  List<Category> getChildren(@Param("id") int id);

  @Override
  // @Results(id = "categoryResult", value = {@Result(column = "parent_id", property = "parentId")})
  @SelectProvider(type = CategorySqlBuilder.class, method = "all")
  List<Category> all(@Param("filter") String filter, @Param("limit") Integer limit,
      @Param("offset") Integer offset, @Param("sort") Sort sort, @Param("afterId") Integer afterId);

  @Override
  @SelectProvider(type = CategorySqlBuilder.class, method = "findByIds")
  List<Category> findByIds(@Param("ids") List<Integer> ids);

  @Override
  Optional<Category> findById(@Param("id") Integer id);

  @Override
  Integer lastInsertId();

  @Override
  @SelectProvider(type = CategorySqlBuilder.class, method = "count")
  Long count(@Param("filter") String filter);
}
