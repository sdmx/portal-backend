package org.soenda.portal.data.db.mapper;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.soenda.portal.data.db.Permission;

public interface PermissionMapper {

  /** SELECT */
  @Select("select * from permissions where id=#{id}")
  Optional<Permission> findById(@Param("id") int id);

  @Select("select * from permissions order by name")
  List<Permission> all();

  @MapKey("role_name")
  @Select("select p.*, r.name as role_name from permissions p"
      + " inner join roles_permissions rp on rp.permission_id=p.id"
      + " inner join users_roles ur on ur.role_id=rp.role_id"
      + " inner join roles r on r.id=ur.role_id where ur.user_id=#{userId}")
  Map<String, Set<Permission>> getByUserId(@Param("userId") int userId);

  /** INSERT */
  @Insert("insert into permissions (name, description) values (#{data.name}, #{data.description})")
  int insert(@Param("data") Permission data);

  /** UPDATE */
  @Update("update permissions set name=#{data.name}, description=#{data.description} where id=#{id}")
  int update(@Param("id") int id, @Param("data") Permission data);

  /** DELETE */
  @Delete("delete from permissions where id=#{id}")
  int delete(@Param("id") int id);

  @Insert("delete from permissions")
  int clear();
}
