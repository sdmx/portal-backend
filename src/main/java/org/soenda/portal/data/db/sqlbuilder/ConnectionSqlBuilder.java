package org.soenda.portal.data.db.sqlbuilder;

import java.util.List;

import org.apache.ibatis.jdbc.SQL;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import lombok.AllArgsConstructor;
import lombok.Data;

public class ConnectionSqlBuilder {
  @Data
  @AllArgsConstructor
  public static class TableDefinition {
    String id;
    String tableName;
    String[] filterFields;
  }

  public static String all(TableDefinition tableDef, String filter, Integer limit, Integer offset,
      Sort sort, Integer afterId) {
    return new SQL() {
      {
        SELECT("*");
        FROM(tableDef.getTableName());

        if (filter != null && filter.length() > 0) {
          OR();

          for (String field : tableDef.getFilterFields()) {
            WHERE(String.format("lower(%s) like #{filter}", field));
          }
        }

        if (afterId != null) {
          AND().WHERE(String.format("%s < #{afterId}", tableDef.getId()));
          ORDER_BY(String.format("%s desc", tableDef.getId()));
        } else if (sort != null) {
          sort.stream().forEach(order -> {
            String direction = order.getDirection().equals(Direction.DESC) ? "desc" : "asc";
            ORDER_BY(String.format("%s %s", order.getProperty(), direction));
          });
        }

        if (limit != null) {
          LIMIT(limit);
        }

        if (offset != null && offset > 0) {
          OFFSET(offset);
        }
      }
    }.toString();
  }

  public static String count(TableDefinition tableDef, String filter) {
    return new SQL() {
      {
        SELECT("count(*)");
        FROM(tableDef.getTableName());

        if (filter != null && filter.length() > 0) {
          for (String field : tableDef.getFilterFields()) {
            WHERE(String.format("lower(%s) like #{filter}", field));
          }
        }
      }
    }.toString();
  }

  public static String findByIds(TableDefinition tableDef, final List<Integer> ids) {
    return new SQL() {
      {
        SELECT("*");
        FROM(tableDef.getTableName());

        int idsSize = ids.size();

        if (idsSize > 0) {
          StringBuilder idsJoin = new StringBuilder(idsSize);

          for (int i = idsSize - 1; i >= 0; i--) {
            idsJoin.append(ids.get(i).toString() + (i > 0 ? "," : ""));
          }

          WHERE(String.format("%s in (%s)", tableDef.getId(), idsJoin.toString()));
        }

        ORDER_BY(tableDef.getId());
      }
    }.toString();
  }
}
