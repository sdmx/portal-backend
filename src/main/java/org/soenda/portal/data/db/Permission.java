package org.soenda.portal.data.db;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Permission implements java.io.Serializable {
  private static final long serialVersionUID = 1L;

  private Integer id;
  private String name;
  private String description;

  public Permission(String name, String description) {
    this.name = name;
    this.description = description;
  }
}
