package org.soenda.portal.data.db.sqlbuilder;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;
import org.soenda.portal.data.db.sqlbuilder.ConnectionSqlBuilder.TableDefinition;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

public class PublicationSqlBuilder {
  public static TableDefinition tableDef =
      new TableDefinition("id", "publications", new String[] {"name"});

  public static String all(@Param("filter") String filter, @Param("limit") Integer limit,
      @Param("offset") Integer offset, @Param("sort") Sort sort, @Param("afterId") Integer afterId,
      @Param("userId") Integer userId, @Param("isPublished") Boolean isPublished,
      @Param("status") Character status, @Param("privilege") Character privilege) {
    String query = new SQL() {
      {
        SELECT("*");
        FROM(tableDef.getTableName());

        if (filter != null && filter.length() > 0) {
          OR();

          for (String field : tableDef.getFilterFields()) {
            WHERE(String.format("lower(%s) like #{filter}", field));
          }
        }

        if (userId != null) {
          AND().WHERE("user_id = #{userId}");
        }

        if (isPublished != null) {
          AND().WHERE(String.format("is_published = '%d'", isPublished ? 1 : 0));
        }

        if (status != null) {
          AND().WHERE("status = #{status}");
        }

        if (privilege != null) {
          AND().WHERE("privilege = #{privilege}");
        }

        if (afterId != null) {
          AND().WHERE(String.format("%s < #{afterId}", tableDef.getId()));
          ORDER_BY(String.format("%s desc", tableDef.getId()));
        } else if (sort != null) {
          sort.stream().forEach(order -> {
            String direction = order.getDirection().equals(Direction.DESC) ? "desc" : "asc";
            ORDER_BY(String.format("%s %s", order.getProperty(), direction));
          });
        }

        if (limit != null) {
          LIMIT(limit);
        }

        if (offset != null && offset > 0) {
          OFFSET(offset);
        }
      }
    }.toString();

    return query;
  }

  public static String count(@Param("filter") String filter, @Param("userId") Integer userId,
      @Param("isPublished") Boolean isPublished, @Param("status") Character status,
      @Param("privilege") Character privilege) {
    return new SQL() {
      {
        SELECT("count(*)");
        FROM(tableDef.getTableName());

        if (filter != null && filter.length() > 0) {
          for (String field : tableDef.getFilterFields()) {
            WHERE(String.format("lower(%s) like #{filter}", field));
          }
        }

        if (userId != null) {
          AND().WHERE("user_id = #{userId}");
        }

        if (isPublished != null) {
          AND().WHERE("is_published = #{isPublished}");
        }

        if (status != null) {
          AND().WHERE("status = #{status}");
        }

        if (privilege != null) {
          AND().WHERE("privilege = #{privilege}");
        }
      }
    }.toString();
  }

  public static String findByIds(@Param("ids") final List<Integer> ids) {
    return ConnectionSqlBuilder.findByIds(tableDef, ids);
  }
}
