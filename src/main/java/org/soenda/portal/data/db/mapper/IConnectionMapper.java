package org.soenda.portal.data.db.mapper;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Sort;

public interface IConnectionMapper<T, I> {
  List<T> all(String filter, Integer limit, Integer offset, Sort sort, Integer afterId);

  List<T> findByIds(List<I> ids);

  Optional<T> findById(I id);

  I lastInsertId();

  Long count(String filter);
}
