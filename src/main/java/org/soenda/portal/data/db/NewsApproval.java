package org.soenda.portal.data.db;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

@Data
@NoArgsConstructor
@EqualsAndHashCode
public class NewsApproval implements java.io.Serializable {
  private static final long serialVersionUID = 1L;

  private Long id;
  private User user;
  private News news;
  private char status;
  private DateTime created;
  private DateTime updated;
}
