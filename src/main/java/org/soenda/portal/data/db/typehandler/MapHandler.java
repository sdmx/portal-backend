package org.soenda.portal.data.db.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

@MappedTypes({Map.class})
public class MapHandler implements TypeHandler<Map<String, String>> {
  private ObjectMapper mapper;
  private ObjectReader reader;

  public MapHandler() {
    mapper = new ObjectMapper();
    reader = mapper.readerFor(new TypeReference<Map<String, String>>() {
    });
  }

  @Override
  public void setParameter(PreparedStatement ps, int i, Map<String, String> value, JdbcType jdbcType)
      throws SQLException {
    try {
      ps.setString(i, value == null ? null : mapper.writeValueAsString(value));
    } catch (Exception e) {
      throw new SQLException(e.getMessage());
    }
  }

  @Override
  public Map<String, String> getResult(ResultSet rs, String columnName) throws SQLException {
    return parse(rs.getString(columnName));
  }

  @Override
  public Map<String, String> getResult(ResultSet rs, int columnIndex) throws SQLException {
    return parse(rs.getString(columnIndex));
  }

  @Override
  public Map<String, String> getResult(CallableStatement cs, int columnIndex) throws SQLException {
    return parse(cs.getString(columnIndex));
  }

  private Map<String, String> parse(String value) throws SQLException {
    if (value != null) {
      try {
        return reader.readValue(value);
      } catch (Exception e) {}
    }

    return null;
  }
}
