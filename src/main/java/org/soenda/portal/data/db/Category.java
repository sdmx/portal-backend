package org.soenda.portal.data.db;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Category implements java.io.Serializable {
  private static final long serialVersionUID = 1L;

  Integer id;
  String name;
  Integer parentId;

  public Category(String name, Integer parentId) {
    this.name = name;
    this.parentId = parentId;
  }

  public boolean hasParent() {
    return parentId != null;
  }
}
