package org.soenda.portal.data.db.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

@MappedTypes({List.class})
public class ListHandler implements TypeHandler<List<String>> {
  private ObjectMapper mapper;
  private ObjectReader reader;

  public ListHandler() {
    mapper = new ObjectMapper();
    reader = mapper.readerFor(new TypeReference<List<String>>() {});
  }

  @Override
  public void setParameter(PreparedStatement ps, int i, List<String> value, JdbcType jdbcType)
      throws SQLException {
    try {
      ps.setString(i, value == null ? null : mapper.writeValueAsString(value));
    } catch (Exception e) {
      throw new SQLException(e.getMessage());
    }
  }

  @Override
  public List<String> getResult(ResultSet rs, String columnName) throws SQLException {
    return parse(rs.getString(columnName));
  }

  @Override
  public List<String> getResult(ResultSet rs, int columnIndex) throws SQLException {
    return parse(rs.getString(columnIndex));
  }

  @Override
  public List<String> getResult(CallableStatement cs, int columnIndex) throws SQLException {
    return parse(cs.getString(columnIndex));
  }

  private List<String> parse(String value) throws SQLException {
    if (value != null) {
      try {
        return reader.readValue(value);
      } catch (Exception e) {}
    }

    return null;
  }
}
