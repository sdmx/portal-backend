package org.soenda.portal.data.db.repository;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.apache.commons.io.FilenameUtils;
import org.joda.time.DateTime;
import org.soenda.portal.data.FileData;
import org.soenda.portal.data.db.Category;
import org.soenda.portal.data.db.Publication;
import org.soenda.portal.data.db.mapper.PublicationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@Repository
public class PublicationRepository {
  private DataSourceTransactionManager transactionManager;
  private PublicationMapper publicationMapper;

  @Autowired
  public PublicationRepository(DataSourceTransactionManager transactionManager, PublicationMapper publicationMapper) {
    this.transactionManager = transactionManager;
    this.publicationMapper = publicationMapper;
  }

  public Publication create(Publication data) {
    TransactionStatus txStatus =
        transactionManager.getTransaction(new DefaultTransactionDefinition());

    try {
      publicationMapper.insert(data);

      Long id = publicationMapper.lastInsertId();
      data.setId(id);
      data.setCreated(DateTime.now());
      data.setUpdated(DateTime.now());

      addCategories(id, data.getCategories());
      uploadThumbnail(data.getThumbnail());

      transactionManager.commit(txStatus);

      return data;
    } catch (Exception e) {
      transactionManager.rollback(txStatus);
      e.printStackTrace();
    }

    return null;
  }

  public Publication update(Long id, Publication data) {
    Optional<Publication> publication = publicationMapper.findById(id);
    TransactionStatus txStatus =
        transactionManager.getTransaction(new DefaultTransactionDefinition());

    if (publication.isPresent()) {
      try {
        publicationMapper.update(id, data);
        data.setId(id);
        data.setUpdated(DateTime.now());

        // categories
        publicationMapper.deleteAllCategories(id);
        addCategories(id, data.getCategories());

        // upload new thumbnail
        FileData thumbnail = data.getThumbnail();

        if (thumbnail != null) {
          uploadThumbnail(thumbnail);

          // delete old thumbnail
          FileData oldThumbnail = publication.get().getThumbnail();

          if (oldThumbnail != null) {
            oldThumbnail.delete();
          }
        }

        transactionManager.commit(txStatus);

        return data;
      } catch (Exception e) {
        transactionManager.rollback(txStatus);
        e.printStackTrace();
      }
    }

    return null;
  }

  public Publication delete(Long id) {
    Optional<Publication> publication = publicationMapper.findById(id);
    TransactionStatus txStatus =
        transactionManager.getTransaction(new DefaultTransactionDefinition());

    if (publication.isPresent()) {
      try {
        publicationMapper.deleteAllCategories(id);
        publicationMapper.delete(id);

        transactionManager.commit(txStatus);

        // delete thumbnail
        FileData thumbnail = publication.get().getThumbnail();

        if (thumbnail != null) {
          thumbnail.delete();
        }

        return publication.get();
      } catch (Exception e) {
        transactionManager.rollback(txStatus);
        e.printStackTrace();
      }
    }

    return null;
  }

  private void uploadThumbnail(FileData thumbnail) throws IOException {
    if (thumbnail != null) {
      String filename = String.format("publication/%d-%s.%s", DateTime.now().getMillis(),
          UUID.randomUUID().toString(), FilenameUtils.getExtension(thumbnail.getFilename()));
      thumbnail.copyTo(filename);
    }
  }

  private void addCategories(Long id, List<Category> categories) {
    for (Category category : categories) {
      publicationMapper.addCategory(id, category.getId());
    }
  }
}
