package org.soenda.portal.data.db.enums;

import lombok.Getter;

public enum Status {
  INACTIVE('0'), ACTIVE('1');

  @Getter
  private final char value;

  private Status(char value) {
    this.value = value;
  }

  public static Status get(char value) {
    for (Status status : Status.values()) {
      if (status.getValue() == value) {
        return status;
      }
    }

    // default status
    return INACTIVE;
  }
}
