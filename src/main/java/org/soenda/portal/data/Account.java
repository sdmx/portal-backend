package org.soenda.portal.data;

import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.soenda.portal.data.db.Role;
import org.soenda.portal.data.db.User;

@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Account implements java.io.Serializable {
  private static final long serialVersionUID = 1L;

  private User user;
  private List<Role> roles;
}
