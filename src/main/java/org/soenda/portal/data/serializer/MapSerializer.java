package org.soenda.portal.data.serializer;

import java.io.IOException;
import java.util.Map;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MapSerializer {
  public static class Deserializer extends JsonDeserializer<Map> {
    @Override
    public Map deserialize(JsonParser jp, DeserializationContext ctxt)
        throws IOException, JsonProcessingException {
      TypeReference<Map> typeRef = new TypeReference<Map>(){};
      return new ObjectMapper().convertValue(jp.readValueAsTree(), typeRef);
    }
  }
}
