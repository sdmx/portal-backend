package org.soenda.portal.data.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

public class DateTimeSerializer {

  public static class Serializer extends StdSerializer<DateTime> {
    private static final long serialVersionUID = 1L;

    public Serializer() {
      super(DateTime.class);
    }

    @Override
    public void serialize(DateTime value, JsonGenerator gen, SerializerProvider provider)
        throws IOException {
      if (value == null) {
        gen.writeNull();
      } else {
        gen.writeString(ISODateTimeFormat.dateTime().withZoneUTC().print(value));
      }
    }
  }

  public static class Deserializer extends StdDeserializer<DateTime> {
    private static final long serialVersionUID = 1L;

    public Deserializer() {
      super(DateTime.class);
    }

    @Override
    public DateTime deserialize(JsonParser parser, DeserializationContext ctx)
        throws IOException, JsonProcessingException {
      final String strDate = parser.getText();
      DateTime parsedDate = null;

      if (strDate == null || strDate.trim().isEmpty()) {
        return parsedDate;
      }

      return DateTime.parse(strDate);
    }
  }
}
