package org.soenda.portal.data;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;
import org.apache.commons.io.FilenameUtils;
import org.joda.time.DateTime;
import org.soenda.portal.config.AppConfig;
import org.springframework.web.multipart.MultipartFile;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class FileData {
  @Getter
  @Setter
  private Path filepath;

  @Getter
  @Setter
  private String filename;

  @Setter
  private byte[] bytes;

  public FileData(String filepath) {
    this.filepath = AppConfig.storagePath.resolve(filepath);
  }

  public FileData(MultipartFile file) throws IOException {
    this.bytes = file.getBytes();
    this.filename = file.getOriginalFilename();
  }

  public void copyWithGeneratedFilename(String dirname) throws IOException {
    if (this.filename != null) {
      String extension = FilenameUtils.getExtension(this.filename);
      String filename = DateTime.now().toString("YYMMdd") + "-" + UUID.randomUUID().toString();
      this.copyTo(dirname + "/" + filename + "." + extension);
    }
  }

  public void copyTo(String filepath) throws IOException {
    // upload file
    if (bytes.length > 0) {
      this.filepath = AppConfig.storagePath.resolve(filepath);

      // make directory if not exists
      File dir = this.filepath.getParent().toFile();

      if (!dir.exists()) {
        dir.mkdirs();
      }

      // copy file
      File destFile = this.filepath.toFile();
      this.filename = destFile.getName();

      BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(destFile));
      stream.write(bytes);
      stream.close();
    }
  }

  public void delete() {
    File file = this.filepath.toFile();

    if (file.exists()) {
      file.delete();
    }
  }

  public String getPath() {
    if (filepath != null) {
      return AppConfig.storagePath.relativize(filepath).toString();
    }

    return null;
  }

  public byte[] getBytes() throws IOException {
    if (this.filepath != null) {
      this.bytes = Files.readAllBytes(this.filepath);
    }

    return this.bytes;
  }

  public String toString() {
    if (this.filepath != null) {
      return this.filepath.toString();
    } else if (this.filename != null) {
      return this.filename;
    }

    return "";
  }
}
