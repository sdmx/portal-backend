package org.soenda.portal.graphql;

import javax.annotation.PostConstruct;
import org.dataloader.DataLoader;
import org.soenda.portal.graphql.data.CategoryDto;
import org.soenda.portal.graphql.data.NewsDto;
import org.soenda.portal.graphql.data.PublicationDto;
import org.soenda.portal.graphql.data.UserDto;
import org.soenda.portal.graphql.datafetcher.CategoryDataFetcher;
import org.soenda.portal.graphql.datafetcher.NewsDataFetcher;
import org.soenda.portal.graphql.datafetcher.PublicationDataFetcher;
import org.soenda.portal.graphql.datafetcher.ViewerDataFetcher;
import org.soenda.portal.graphql.scalar.DateTimeScalar;
import org.soenda.portal.graphql.scalar.FileScalar;
import org.soenda.portal.graphql.type.NodeId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import graphql.TypeResolutionEnvironment;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import graphql.schema.TypeResolver;
import graphql.schema.idl.RuntimeWiring;
import lombok.Getter;

@Service
public class GraphQLWiring {
  @Getter
  RuntimeWiring runtimeWiring;

  ViewerDataFetcher viewerDataFetcher;
  CategoryDataFetcher categoryDataFetcher;
  NewsDataFetcher newsDataFetcher;
  PublicationDataFetcher publicationDataFetcher;

  /** Viewer */
  private void buildViewer(RuntimeWiring.Builder builder) {
    builder.type("Viewer", typeWiring -> {
      // Authenticated User
      typeWiring.dataFetcher("user", viewerDataFetcher.user());

      // Category
      typeWiring.dataFetcher("category", categoryDataFetcher.category())
          .dataFetcher("allCategories", categoryDataFetcher.allCategories());

      // News
      typeWiring.dataFetcher("news", newsDataFetcher.news())
          .dataFetcher("allNews", newsDataFetcher.allNews());

      // Publication
      typeWiring.dataFetcher("publication", publicationDataFetcher.publication())
          .dataFetcher("allPublications", publicationDataFetcher.allPublications());

      return typeWiring;
    });
  }

  /** Types */
  private void buildTypes(RuntimeWiring.Builder builder) {
    builder.type("Category", typeWiring -> {
      return typeWiring.dataFetcher("parent", categoryDataFetcher.parent())
          .dataFetcher("children", categoryDataFetcher.children());
    });

    builder.type("News", typeWiring -> {
      return typeWiring.dataFetcher("user", newsDataFetcher.user())
          .dataFetcher("categories", newsDataFetcher.categories());
    });

    builder.type("Publication", typeWiring -> {
      return typeWiring.dataFetcher("user", publicationDataFetcher.user())
          .dataFetcher("categories", publicationDataFetcher.categories());
    });
  }

  /** Mutation */
  private void buildMutation(RuntimeWiring.Builder builder) {
    builder.type("Mutation", typeWiring -> {
      // Category
      typeWiring.dataFetcher("createCategory", categoryDataFetcher.createCategory())
          .dataFetcher("updateCategory", categoryDataFetcher.updateCategory())
          .dataFetcher("deleteCategory", categoryDataFetcher.deleteCategory());

      // News
      typeWiring.dataFetcher("createNews", newsDataFetcher.createNews())
          .dataFetcher("updateNews", newsDataFetcher.updateNews())
          .dataFetcher("deleteNews", newsDataFetcher.deleteNews());

      // Publication
      typeWiring.dataFetcher("createPublication", publicationDataFetcher.createPublication())
          .dataFetcher("updatePublication", publicationDataFetcher.updatePublication())
          .dataFetcher("deletePublication", publicationDataFetcher.deletePublication());

      return typeWiring;
    });
  }

  /** RootQuery */
  private void buildRootQuery(RuntimeWiring.Builder builder) {
    builder.type("RootQuery", typeWiring -> {
      typeWiring.dataFetcher("viewer", viewerDataFetcher.viewer());
      typeWiring.dataFetcher("node", new DataFetcher<Object>() {
        @Override
        public Object get(DataFetchingEnvironment env) throws Exception {
          NodeId<Object> nodeId = new NodeId<>(env.getArgument("id"));
          DataLoader<Object, Object> dataLoader = env.getDataLoader(nodeId.getEntity());

          if (dataLoader == null) {
            return null;
          }

          return dataLoader.load(nodeId.getId());
        }
      });

      return typeWiring;
    });
  }

  @PostConstruct
  public void build() {
    RuntimeWiring.Builder builder = RuntimeWiring.newRuntimeWiring();
    buildRootQuery(builder);
    buildViewer(builder);
    buildTypes(builder);
    buildMutation(builder);

    // Scalars
    builder.scalar(DateTimeScalar.DateTime);
    builder.scalar(FileScalar.File);

    // Interfaces & Unions
    builder.type("Node", typeWiring -> {
      return typeWiring.typeResolver(new TypeResolver() {
        @Override
        public GraphQLObjectType getType(TypeResolutionEnvironment env) {
          GraphQLSchema schema = env.getSchema();
          Object javaObject = env.getObject();

          if (javaObject instanceof CategoryDto) {
            return schema.getObjectType("Category");
          } else if (javaObject instanceof NewsDto) {
            return schema.getObjectType("News");
          } else if (javaObject instanceof PublicationDto) {
            return schema.getObjectType("Publication");
          } else if (javaObject instanceof UserDto) {
            return schema.getObjectType("User");
          }

          return null;
        }
      });
    });

    runtimeWiring = builder.build();
  }

  @Autowired
  public void setViewerDataFetcher(ViewerDataFetcher viewerDataFetcher) {
    this.viewerDataFetcher = viewerDataFetcher;
  }

  @Autowired
  public void setCategoryDataFetcher(CategoryDataFetcher categoryDataFetcher) {
    this.categoryDataFetcher = categoryDataFetcher;
  }

  @Autowired
  public void setNewsDataFetcher(NewsDataFetcher newsDataFetcher) {
    this.newsDataFetcher = newsDataFetcher;
  }

  @Autowired
  public void setPublicationDataFetcher(PublicationDataFetcher publicationDataFetcher) {
    this.publicationDataFetcher = publicationDataFetcher;
  }
}
