package org.soenda.portal.graphql;

import java.util.Map;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.soenda.portal.data.serializer.MapSerializer;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GraphQLQuery {
  String query;
  String operationName;

  @JsonDeserialize(using = MapSerializer.Deserializer.class)
  Map<String, Object> variables;

  public Boolean hasVariables() {
    return variables != null && variables.size() > 0;
  }
}
