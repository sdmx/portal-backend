package org.soenda.portal.graphql.scalar;

import java.io.IOException;
import java.util.List;
import org.soenda.portal.data.FileData;
import org.springframework.web.multipart.MultipartFile;
import graphql.language.StringValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseLiteralException;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;

public class FileScalar {
  public static final GraphQLScalarType File =
      GraphQLScalarType.newScalar().name("File").coercing(new Coercing<FileData, String>() {
        @Override
        public String serialize(final Object dataFetcherResult) {
          if (dataFetcherResult instanceof MultipartFile) {
            try {
              return new FileData((MultipartFile) dataFetcherResult).getPath();
            } catch (IOException e) {
              e.printStackTrace();
            }
          } else if (dataFetcherResult instanceof FileData) {
            return ((FileData) dataFetcherResult).getPath();
          }

          throw new CoercingSerializeException("Unable to serialize value as a file.");
        }

        @Override
        public FileData parseValue(final Object input) {
          if (input instanceof FileData) {
            return (FileData) input;
          } else if (input instanceof String) {
            return new FileData(input.toString());
          } else if (input instanceof List) {
            throw new CoercingParseValueException("Input file are not multiple.");
          }

          throw new CoercingParseValueException(
              "Unable to parse variable value '" + input + "' as an file.");
        }

        @Override
        public FileData parseLiteral(final Object input) {
          if (input instanceof StringValue) {
            String value = ((StringValue) input).getValue();

            if (!value.trim().isEmpty()) {
              return new FileData(value);
            }
          }

          throw new CoercingParseLiteralException(
              "Value is not any file: '" + String.valueOf(input) + "'");
        }
      }).build();
}
