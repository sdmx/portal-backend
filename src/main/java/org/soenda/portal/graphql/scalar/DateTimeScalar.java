package org.soenda.portal.graphql.scalar;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import graphql.language.StringValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseLiteralException;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;

public class DateTimeScalar {
  public static final GraphQLScalarType DateTime =
      GraphQLScalarType.newScalar().name("DateTime").coercing(new Coercing<DateTime, String>() {
        @Override
        public String serialize(final Object dataFetcherResult) {
          if (dataFetcherResult instanceof DateTime) {
            final DateTime value = (DateTime) dataFetcherResult;
            return ISODateTimeFormat.dateTime().withZoneUTC().print(value);
          } else if (dataFetcherResult instanceof String) {
            return dataFetcherResult.toString();
          }

          throw new CoercingSerializeException("Unable to serialize value as a date time.");
        }

        @Override
        public DateTime parseValue(final Object input) {
          try {
            if (input instanceof String) {
              String value = input.toString();

              if (!value.trim().isEmpty()) {
                return org.joda.time.DateTime.parse(value);
              }
            }
          } catch (Exception e) {
            e.printStackTrace();
          }

          throw new CoercingParseValueException(
              "Unable to parse variable value '" + input + "' as an date time.");
        }

        @Override
        public DateTime parseLiteral(final Object input) {
          if (input instanceof StringValue) {
            String value = ((StringValue) input).getValue();

            if (!value.trim().isEmpty()) {
              return org.joda.time.DateTime.parse(value);
            }
          }

          throw new CoercingParseLiteralException(
              "Value is not any date time: '" + String.valueOf(input) + "'");
        }
      }).build();
}
