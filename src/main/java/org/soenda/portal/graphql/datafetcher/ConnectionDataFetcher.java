package org.soenda.portal.graphql.datafetcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import com.google.common.base.Objects;
import org.soenda.portal.data.db.mapper.IConnectionMapper;
import org.soenda.portal.graphql.datafetcher.inputhandler.FilterQueryInputHandler;
import org.soenda.portal.graphql.type.ConnectionImpl;
import org.soenda.portal.graphql.type.INode;
import org.soenda.portal.graphql.type.NodeId;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import graphql.relay.Connection;
import graphql.relay.ConnectionCursor;
import graphql.relay.DefaultConnectionCursor;
import graphql.relay.DefaultEdge;
import graphql.relay.DefaultPageInfo;
import graphql.relay.Edge;
import graphql.relay.PageInfo;
import graphql.schema.DataFetchingEnvironment;

public class ConnectionDataFetcher<T, I, D> {
  IConnectionMapper<T, I> mapper;
  Class<T> nodeType;
  Class<D> dtoType;
  Function<Void, Map<String, Object>> initFunc;
  TotalCountFunction<T, I> totalCountFunc;
  FindDataFunction<T, I> findDataFunc;

  public ConnectionDataFetcher(IConnectionMapper<T, I> mapper, Class<T> nodeType,
      Class<D> dtoType) {
    this.mapper = mapper;
    this.nodeType = nodeType;
    this.dtoType = dtoType;
  }

  public Connection<D> all(DataFetchingEnvironment env) throws Exception {
    final String filter = new FilterQueryInputHandler().handle(env.getArgument("filter"));
    final Integer first = env.getArgument("first");
    final Integer offset = env.getArgument("offset");
    final NodeId<Integer> afterNodeId = new NodeId<>(env.getArgument("after"));
    final Integer afterId = afterNodeId.getKey(Integer.class);
    final Map<String, String> order = env.getArgument("order");

    final Integer limit = first == null ? null : first + 1;

    // default sorting, order by id desc
    Sort sort = Sort.by(Direction.DESC, "id");

    if (order != null) {
      final Direction direction =
          Objects.equal(order.get("direction"), "DESC") ? Direction.DESC : Direction.ASC;
      sort = Sort.by(direction, order.get("by"));
    }

    // fetch data from database
    List<T> data = findDataFunc != null
        ? findDataFunc.apply(mapper, filter, limit, offset, sort, afterId)
        : mapper.all(filter, limit, offset, sort, afterId);

    Integer count = data.size();

    if (count == 0) {
      return null;
    }

    final boolean hasNext = first != null && count > first;
    final boolean hasPrev = (offset != null && offset > 0) || afterNodeId.isPresent();

    // remove last item
    // the last item is only used as a marker to assure that there is a next page
    if (hasNext) {
      data.remove(count - 1);
    }

    // make edges
    List<Edge<D>> edges = new ArrayList<>();

    for (T item : data) {
      D dto = dtoType.getConstructor(nodeType).newInstance(item);
      edges.add(new DefaultEdge<D>(dto, new DefaultConnectionCursor(((INode<T>) dto).getId())));
    }

    count = edges.size();

    // make pageinfo
    ConnectionCursor startCursor = count <= 0 ? null : edges.get(0).getCursor();
    ConnectionCursor endCursor = count <= 0 ? null : edges.get(count - 1).getCursor();
    final PageInfo pageInfo = new DefaultPageInfo(startCursor, endCursor, hasPrev, hasNext);

    ConnectionImpl<D> connection = new ConnectionImpl<>(edges, pageInfo);

    // calculate total count with filter
    if (env.getSelectionSet().contains("totalCount")) {
      Long totalCount =
          totalCountFunc != null ? totalCountFunc.apply(mapper, filter) : mapper.count(filter);

      connection.setTotalCount(totalCount);
    }

    return connection;
  }

  public void init(Function<Void, Map<String, Object>> initFunc) {
    this.initFunc = initFunc;
  }

  public void onFindData(FindDataFunction<T, I> findDataFunc) {
    this.findDataFunc = findDataFunc;
  }

  public void onTotalCount(TotalCountFunction<T, I> totalCountFunc) {
    this.totalCountFunc = totalCountFunc;
  }

  public static interface TotalCountFunction<T, I> {
    public Long apply(IConnectionMapper<T, I> mapper, String filter);
  }

  public static interface FindDataFunction<T, I> {
    public List<T> apply(IConnectionMapper<T, I> mapper, String filter, Integer limit,
        Integer offset, Sort sort, Integer afterId);
  }
}
