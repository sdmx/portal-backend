package org.soenda.portal.graphql.datafetcher;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dataloader.DataLoader;
import org.soenda.portal.data.db.Category;
import org.soenda.portal.data.db.Publication;
import org.soenda.portal.data.db.User;
import org.soenda.portal.data.db.enums.Privilege;
import org.soenda.portal.data.db.enums.Status;
import org.soenda.portal.data.db.mapper.CategoryMapper;
import org.soenda.portal.data.db.mapper.IConnectionMapper;
import org.soenda.portal.data.db.mapper.PublicationMapper;
import org.soenda.portal.data.db.repository.PublicationRepository;
import org.soenda.portal.graphql.data.CategoryDto;
import org.soenda.portal.graphql.data.PublicationDto;
import org.soenda.portal.graphql.data.PublicationInput;
import org.soenda.portal.graphql.data.UserDto;
import org.soenda.portal.graphql.type.NodeId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;
import graphql.GraphQLContext;
import graphql.relay.Connection;
import graphql.relay.DefaultConnectionCursor;
import graphql.relay.DefaultEdge;
import graphql.relay.Edge;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class PublicationDataFetcher {
  DataLoader<String, PublicationDto> dataLoader;
  DataSourceTransactionManager transactionManager;
  ObjectMapper objectMapper;

  ConnectionDataFetcher<Publication, Long, PublicationDto> connectionDataFetcher;
  CategoryMapper categoryMapper;
  PublicationRepository publicationRepository;
  PublicationMapper publicationMapper;

  @Autowired
  public PublicationDataFetcher(final DataSourceTransactionManager transactionManager,
      final ObjectMapper objectMapper, PublicationRepository publicationRepository,
      final CategoryMapper categoryMapper, final PublicationMapper publicationMapper) {
    this.transactionManager = transactionManager;
    this.objectMapper = objectMapper;
    this.publicationRepository = publicationRepository;

    this.connectionDataFetcher =
        new ConnectionDataFetcher<>(publicationMapper, Publication.class, PublicationDto.class);
    this.categoryMapper = categoryMapper;
    this.publicationMapper = publicationMapper;
  }

  private DataLoader<String, PublicationDto> getDataLoader(final DataFetchingEnvironment env) {
    if (dataLoader == null) {
      dataLoader = env.getDataLoader("publication");
    }

    return dataLoader;
  }

  public DataFetcher<Connection<PublicationDto>> allPublications() {
    return env -> {
      final GraphQLContext ctx = env.getContext();
      final User user = ctx.get("user");

      // ownedByUser
      final Boolean ownedByUser = env.getArgumentOrDefault("ownedByUser", false);
      final Integer userId = ownedByUser && user != null ? user.getId() : null;
      final Boolean isPublished =
          user == null || !ownedByUser ? true : env.getArgument("isPublished");
      final Character privilege =
          user == null ? Privilege.PUBLIC.getValue() : env.getArgument("privilege");

      // status
      final Character status;

      if (user == null || !ownedByUser) {
        status = Status.ACTIVE.getValue();
      } else if (env.containsArgument("status")) {
        final Status statusEnum = Enum.valueOf(Status.class, env.getArgument("status"));
        status = statusEnum.getValue();
      } else {
        status = null;
      }

      connectionDataFetcher
          .onFindData(new ConnectionDataFetcher.FindDataFunction<Publication, Long>() {
            @Override
            public List<Publication> apply(IConnectionMapper<Publication, Long> mapper,
                String filter, Integer limit, Integer offset, Sort sort, Integer afterId) {
              return ((PublicationMapper) mapper).findAll(filter, limit, offset, sort, afterId,
                  userId, isPublished, status, privilege);
            }
          });

      connectionDataFetcher
          .onTotalCount(new ConnectionDataFetcher.TotalCountFunction<Publication, Long>() {
            @Override
            public Long apply(IConnectionMapper<Publication, Long> mapper, String filter) {
              return ((PublicationMapper) mapper).filterCount(filter, userId, isPublished, status,
                  privilege);
            }
          });

      return connectionDataFetcher.all(env);
    };
  }

  public DataFetcher<CompletableFuture<PublicationDto>> publication() {
    return env -> getDataLoader(env).load(env.getArgument("id"));
  }

  public DataFetcher<UserDto> user() {
    return env -> {
      final PublicationDto current = env.getSource();
      final Long id = current.getNode().getId();
      final Optional<User> user = publicationMapper.getUserById(id);

      return user.isPresent() ? new UserDto(user.get()) : null;
    };
  }

  public DataFetcher<List<CategoryDto>> categories() {
    return env -> {
      final PublicationDto current = env.getSource();
      final Long id = current.getNode().getId();
      final List<Category> categories = publicationMapper.getCategoriesById(id);

      return categories != null && categories.size() > 0 ? CategoryDto.transform(categories) : null;
    };
  }

  /** MUTATIONS */
  public DataFetcher<Edge<PublicationDto>> createPublication() {
    return env -> {
      final GraphQLContext ctx = env.getContext();
      final User user = ctx.get("user");

      final PublicationInput input = PublicationInput.parse(objectMapper, env);
      final Publication data = input.getData();
      data.setUserId(user.getId());

      final Publication payload = publicationRepository.create(data);

      return getPayload(payload);
    };
  }

  public DataFetcher<Edge<PublicationDto>> updatePublication() {
    return env -> {
      final PublicationInput input = PublicationInput.parse(objectMapper, env);
      final NodeId<Long> nodeId = new NodeId<>(env.getArgument("id"));
      final Publication payload =
          publicationRepository.update(nodeId.getKey(Long.class), input.getData());

      return getPayload(payload);
    };
  }

  public DataFetcher<Edge<PublicationDto>> deletePublication() {
    return env -> {
      final NodeId<Long> nodeId = new NodeId<>(env.getArgument("id"));
      final Publication payload = publicationRepository.delete(nodeId.getKey(Long.class));

      // remove from data loader
      if (payload != null) {
        getDataLoader(env).clear(nodeId.getId());
      }

      return getPayload(payload);
    };
  }

  private Edge<PublicationDto> getPayload(Publication payload) {
    if (payload == null) {
      return null;
    }

    final PublicationDto dto = new PublicationDto(payload);
    return new DefaultEdge<PublicationDto>(dto, new DefaultConnectionCursor(dto.getId()));
  }
}
