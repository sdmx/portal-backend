package org.soenda.portal.graphql.datafetcher;

import java.util.Optional;
import org.soenda.portal.data.db.User;
import org.soenda.portal.data.db.mapper.UserMapper;
import org.soenda.portal.graphql.data.UserDto;
import org.soenda.portal.graphql.type.Viewer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import graphql.GraphQLContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class ViewerDataFetcher {
  UserMapper userMapper;

  @Autowired
  public ViewerDataFetcher(final UserMapper userMapper) {
    this.userMapper = userMapper;
  }

  public DataFetcher<Viewer> viewer() {
    return env -> {
      return new Viewer(getUser(env));
    };
  }

  public DataFetcher<UserDto> user() {
    return env -> {
      User user = getUser(env);
      return user != null ? new UserDto(user) : null;
    };
  }

  User getUser(DataFetchingEnvironment env) {
    GraphQLContext context = env.getContext();
    return context.get("user");
  }
}
