package org.soenda.portal.graphql.datafetcher.inputhandler;

public class FilterQueryInputHandler implements IInputHandler<String> {
  @Override
  public String handle(Object input) {
    if (input != null) {
      String query = input.toString();

      if (query.length() > 0) {
        return String.format("%%%s%%", query.toLowerCase());
      }
    }

    return null;
  }
}
