package org.soenda.portal.graphql.datafetcher.inputhandler;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class PaginationInputHandler implements IInputHandler<Pageable> {
  final Integer defaultPage = 1;
  final Integer defaultPageSize = 20;

  @Override
  public Pageable handle(Object input) {
    if (input == null) {
      return PageRequest.of(defaultPage - 1, defaultPageSize);
    }

    Map<String, Object> pagination = (Map<String, Object>) input;
    Integer page = Integer.valueOf(pagination.getOrDefault("page", defaultPage).toString()) - 1;
    Integer pageSize = Integer.valueOf(pagination.getOrDefault("size", defaultPageSize).toString());

    if (pagination.containsKey("sort")) {
      List<Object> sortParams = (List<Object>) pagination.get("sort");
      SortInputHandler sortInputHandler = new SortInputHandler();
      List<Sort.Order> sorts = sortParams.stream().map(item -> sortInputHandler.handle(item))
          .collect(Collectors.toList());

      return PageRequest.of(page, pageSize, Sort.by(sorts));
    }

    return PageRequest.of(page, pageSize);
  }
}
