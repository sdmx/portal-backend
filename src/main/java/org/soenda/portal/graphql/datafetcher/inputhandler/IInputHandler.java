package org.soenda.portal.graphql.datafetcher.inputhandler;

public interface IInputHandler<T> {
  public T handle(Object input);
}
