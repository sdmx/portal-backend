package org.soenda.portal.graphql.datafetcher;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dataloader.DataLoader;
import org.soenda.portal.data.db.Category;
import org.soenda.portal.data.db.mapper.CategoryMapper;
import org.soenda.portal.graphql.data.CategoryDto;
import org.soenda.portal.graphql.data.CategoryInput;
import org.soenda.portal.graphql.type.NodeId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;
import graphql.relay.Connection;
import graphql.relay.DefaultConnectionCursor;
import graphql.relay.DefaultEdge;
import graphql.relay.Edge;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class CategoryDataFetcher {
  DataLoader<String, CategoryDto> dataLoader;
  DataSourceTransactionManager transactionManager;
  CategoryMapper categoryMapper;
  ObjectMapper objectMapper;
  ConnectionDataFetcher<Category, Integer, CategoryDto> connectionDataFetcher;

  @Autowired
  public CategoryDataFetcher(final DataSourceTransactionManager transactionManager,
      final CategoryMapper categoryMapper, final ObjectMapper objectMapper) {
    this.transactionManager = transactionManager;
    this.objectMapper = objectMapper;
    this.categoryMapper = categoryMapper;

    this.connectionDataFetcher =
        new ConnectionDataFetcher<>(categoryMapper, Category.class, CategoryDto.class);
  }

  private DataLoader<String, CategoryDto> getDataLoader(final DataFetchingEnvironment env) {
    if (dataLoader == null) {
      dataLoader = env.getDataLoader("category");
    }

    return dataLoader;
  }

  public DataFetcher<CompletableFuture<CategoryDto>> category() {
    return env -> getDataLoader(env).load(env.getArgument("id"));
  }

  public DataFetcher<CompletableFuture<CategoryDto>> parent() {
    return env -> {
      final CategoryDto node = env.getSource();
      final Integer parentId = node.getNode().getParentId();

      if (parentId != null) {
        return getDataLoader(env).load(NodeId.generate("category", parentId));
      }

      return null;
    };
  }

  public DataFetcher<List<CategoryDto>> children() {
    return env -> {
      final CategoryDto current = env.getSource();
      final Integer id = current.getNode().getId();

      return CategoryDto.transform(categoryMapper.getChildren(id));
    };
  }

  public DataFetcher<Connection<CategoryDto>> allCategories() {
    return env -> {
      return connectionDataFetcher.all(env);
    };
  }

  public DataFetcher<Edge<CategoryDto>> createCategory() {
    return env -> {
      final CategoryInput input = CategoryInput.parse(objectMapper, env);
      final Category data = input.getData();

      categoryMapper.insert(data);

      // get last insert data
      data.setId(categoryMapper.lastInsertId());
      CategoryDto dto = new CategoryDto(data);

      return new DefaultEdge<CategoryDto>(dto, new DefaultConnectionCursor(dto.getId()));
    };
  }

  public DataFetcher<Edge<CategoryDto>> updateCategory() {
    return env -> {
      final CategoryInput input = CategoryInput.parse(objectMapper, env);
      final Category data = input.getData();
      final NodeId<Integer> nodeId = new NodeId<>(env.getArgument("id"));
      final Integer id = nodeId.getKey(Integer.class);

      final int res = categoryMapper.update(id, data);
      data.setId(id);

      // clear data loader cache
      getDataLoader(env).clear(nodeId.getId());
      CategoryDto dto = new CategoryDto(data);

      return res > 0
          ? new DefaultEdge<CategoryDto>(dto, new DefaultConnectionCursor(nodeId.getId()))
          : null;
    };
  }

  public DataFetcher<Edge<CategoryDto>> deleteCategory() {
    return env -> {
      final NodeId<Integer> nodeId = new NodeId<>(env.getArgument("id"));
      final Integer id = nodeId.getKey(Integer.class);
      final Optional<Category> data = categoryMapper.findById(id);

      if (!data.isPresent()) {
        return null;
      }

      final int affectedRow = categoryMapper.delete(id);

      // clear data loader cache
      getDataLoader(env).clear(nodeId.getId());
      CategoryDto dto = new CategoryDto(data.get());

      return affectedRow > 0
          ? new DefaultEdge<CategoryDto>(dto, new DefaultConnectionCursor(dto.getId()))
          : null;
    };
  }
}
