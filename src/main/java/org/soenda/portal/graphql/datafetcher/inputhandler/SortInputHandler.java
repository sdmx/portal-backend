package org.soenda.portal.graphql.datafetcher.inputhandler;

import java.util.Map;
import java.util.Objects;
import org.springframework.data.domain.Sort;

public class SortInputHandler implements IInputHandler<Sort.Order> {
  final Integer defaultPage = 1;
  final Integer defaultPageSize = 20;

  @Override
  public Sort.Order handle(Object input) {
    if (input == null) {
      return null;
    }

    Map<String, String> sort = (Map<String, String>) input;
    String fieldName = sort.get("by");
    Boolean isDesc = Objects.equals(sort.get("direction"), "DESC");

    return isDesc ? Sort.Order.desc(fieldName) : Sort.Order.asc(fieldName);
  }
}
