package org.soenda.portal.graphql.datafetcher;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dataloader.DataLoader;
import org.soenda.portal.data.db.Category;
import org.soenda.portal.data.db.News;
import org.soenda.portal.data.db.User;
import org.soenda.portal.data.db.enums.Privilege;
import org.soenda.portal.data.db.enums.Status;
import org.soenda.portal.data.db.mapper.CategoryMapper;
import org.soenda.portal.data.db.mapper.IConnectionMapper;
import org.soenda.portal.data.db.mapper.NewsMapper;
import org.soenda.portal.data.db.repository.NewsRepository;
import org.soenda.portal.graphql.data.CategoryDto;
import org.soenda.portal.graphql.data.NewsDto;
import org.soenda.portal.graphql.data.NewsInput;
import org.soenda.portal.graphql.data.UserDto;
import org.soenda.portal.graphql.type.NodeId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;
import graphql.GraphQLContext;
import graphql.relay.Connection;
import graphql.relay.DefaultConnectionCursor;
import graphql.relay.DefaultEdge;
import graphql.relay.Edge;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class NewsDataFetcher {
  DataLoader<String, NewsDto> dataLoader;
  DataSourceTransactionManager transactionManager;
  ObjectMapper objectMapper;

  ConnectionDataFetcher<News, Long, NewsDto> connectionDataFetcher;
  CategoryMapper categoryMapper;
  NewsRepository newsRepository;
  NewsMapper newsMapper;

  @Autowired
  public NewsDataFetcher(final DataSourceTransactionManager transactionManager,
      final ObjectMapper objectMapper, NewsRepository newsRepository,
      final CategoryMapper categoryMapper, final NewsMapper newsMapper) {
    this.transactionManager = transactionManager;
    this.objectMapper = objectMapper;
    this.newsRepository = newsRepository;

    this.connectionDataFetcher = new ConnectionDataFetcher<>(newsMapper, News.class, NewsDto.class);
    this.categoryMapper = categoryMapper;
    this.newsMapper = newsMapper;
  }

  private DataLoader<String, NewsDto> getDataLoader(final DataFetchingEnvironment env) {
    if (dataLoader == null) {
      dataLoader = env.getDataLoader("news");
    }

    return dataLoader;
  }

  public DataFetcher<Connection<NewsDto>> allNews() {
    return env -> {
      final GraphQLContext ctx = env.getContext();
      final User user = ctx.get("user");

      // ownedByUser
      final Boolean ownedByUser = env.getArgumentOrDefault("ownedByUser", false);
      final Integer userId = ownedByUser && user != null ? user.getId() : null;
      final Boolean isPublished =
          user == null || !ownedByUser ? true : env.getArgument("isPublished");
      final Character privilege =
          user == null ? Privilege.PUBLIC.getValue() : env.getArgument("privilege");

      // status
      final Character status;

      if (user == null || !ownedByUser) {
        status = Status.ACTIVE.getValue();
      } else if (env.containsArgument("status")) {
        final Status statusEnum = Enum.valueOf(Status.class, env.getArgument("status"));
        status = statusEnum.getValue();
      } else {
        status = null;
      }

      connectionDataFetcher.onFindData(new ConnectionDataFetcher.FindDataFunction<News, Long>() {
        @Override
        public List<News> apply(IConnectionMapper<News, Long> mapper, String filter, Integer limit,
            Integer offset, Sort sort, Integer afterId) {
          return ((NewsMapper) mapper).findAll(filter, limit, offset, sort, afterId, userId,
              isPublished, status, privilege);
        }
      });

      connectionDataFetcher
          .onTotalCount(new ConnectionDataFetcher.TotalCountFunction<News, Long>() {
            @Override
            public Long apply(IConnectionMapper<News, Long> mapper, String filter) {
              return ((NewsMapper) mapper).filterCount(filter, userId, isPublished, status,
                  privilege);
            }
          });

      return connectionDataFetcher.all(env);
    };
  }

  public DataFetcher<CompletableFuture<NewsDto>> news() {
    return env -> getDataLoader(env).load(env.getArgument("id"));
  }

  public DataFetcher<UserDto> user() {
    return env -> {
      final NewsDto current = env.getSource();
      final Long id = current.getNode().getId();
      final Optional<User> user = newsMapper.getUserById(id);

      return user.isPresent() ? new UserDto(user.get()) : null;
    };
  }

  public DataFetcher<List<CategoryDto>> categories() {
    return env -> {
      final NewsDto current = env.getSource();
      final Long id = current.getNode().getId();
      final List<Category> categories = newsMapper.getCategoriesById(id);

      return categories != null && categories.size() > 0 ? CategoryDto.transform(categories) : null;
    };
  }

  /** MUTATIONS */
  public DataFetcher<Edge<NewsDto>> createNews() {
    return env -> {
      final GraphQLContext ctx = env.getContext();
      final User user = ctx.get("user");

      final NewsInput input = NewsInput.parse(objectMapper, env);
      final News data = input.getData();
      data.setUserId(user.getId());

      final News payload = newsRepository.create(data);

      return getPayload(payload);
    };
  }

  public DataFetcher<Edge<NewsDto>> updateNews() {
    return env -> {
      final NewsInput input = NewsInput.parse(objectMapper, env);
      final NodeId<Long> nodeId = new NodeId<>(env.getArgument("id"));
      final News payload = newsRepository.update(nodeId.getKey(Long.class), input.getData());

      return getPayload(payload);
    };
  }

  public DataFetcher<Edge<NewsDto>> deleteNews() {
    return env -> {
      final NodeId<Long> nodeId = new NodeId<>(env.getArgument("id"));
      final News payload = newsRepository.delete(nodeId.getKey(Long.class));

      // remove from data loader
      if (payload != null) {
        getDataLoader(env).clear(nodeId.getId());
      }

      return getPayload(payload);
    };
  }

  private Edge<NewsDto> getPayload(News payload) {
    if (payload == null) {
      return null;
    }

    final NewsDto dto = new NewsDto(payload);
    return new DefaultEdge<NewsDto>(dto, new DefaultConnectionCursor(dto.getId()));
  }
}
