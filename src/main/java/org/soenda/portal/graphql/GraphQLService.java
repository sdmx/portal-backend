package org.soenda.portal.graphql;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import javax.annotation.PostConstruct;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.apache.commons.io.FileUtils;
import org.soenda.portal.graphql.instrumentation.AuthInstrumentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import graphql.ExecutionInput;
import graphql.GraphQL;
import graphql.execution.instrumentation.ChainedInstrumentation;
import graphql.execution.instrumentation.Instrumentation;
import graphql.execution.preparsed.PreparsedDocumentEntry;
import graphql.execution.preparsed.PreparsedDocumentProvider;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

@Service
public class GraphQLService {
  final String schemaPath = "graphql/schema.graphql";
  GraphQLWiring graphQLWiring;
  GraphQL graphQL;

  @PostConstruct
  public void init() throws IOException {
    graphQL = loadSchema(new ClassPathResource(schemaPath).getFile());
  }

  public GraphQL loadSchema(File schemaFile) throws IOException {
    TypeDefinitionRegistry typeRegistry =
        new SchemaParser().parse(FileUtils.readFileToString(schemaFile, StandardCharsets.UTF_8));
    GraphQLSchema schema =
        new SchemaGenerator().makeExecutableSchema(typeRegistry, graphQLWiring.getRuntimeWiring());

    // query caching
    Cache<String, PreparsedDocumentEntry> cache = Caffeine.newBuilder().maximumSize(10_000).build();

    PreparsedDocumentProvider preparsedCache = new PreparsedDocumentProvider() {
      @Override
      public PreparsedDocumentEntry getDocument(ExecutionInput executionInput,
          Function<ExecutionInput, PreparsedDocumentEntry> computeFunction) {
        return cache.get(executionInput.getQuery(), key -> {
          return computeFunction.apply(executionInput);
        });
      }
    };

    // instrumentations
    List<Instrumentation> instrumentationList = new ArrayList<>();
    instrumentationList.add(new AuthInstrumentation());
    ChainedInstrumentation instrumentations = new ChainedInstrumentation(instrumentationList);

    return GraphQL.newGraphQL(schema).preparsedDocumentProvider(preparsedCache)
        .instrumentation(instrumentations).build();
  }

  @Autowired
  public void setGraphQLWiring(GraphQLWiring graphQLWiring) {
    this.graphQLWiring = graphQLWiring;
  }

  @Bean
  public GraphQL graphQL() {
    return graphQL;
  }
}
