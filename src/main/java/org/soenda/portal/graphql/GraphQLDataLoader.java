package org.soenda.portal.graphql;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.dataloader.BatchLoader;
import org.dataloader.DataLoader;
import org.dataloader.DataLoaderRegistry;
import org.soenda.portal.data.db.Category;
import org.soenda.portal.data.db.News;
import org.soenda.portal.data.db.Publication;
import org.soenda.portal.data.db.mapper.CategoryMapper;
import org.soenda.portal.data.db.mapper.NewsMapper;
import org.soenda.portal.data.db.mapper.PublicationMapper;
import org.soenda.portal.graphql.data.CategoryDto;
import org.soenda.portal.graphql.data.NewsDto;
import org.soenda.portal.graphql.data.PublicationDto;
import org.soenda.portal.graphql.type.NodeId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.Getter;

@Component
public class GraphQLDataLoader {
  public static final Logger log = Logger.getLogger(GraphQLDataLoader.class);

  @Getter
  DataLoaderRegistry registry = new DataLoaderRegistry();
  CategoryMapper categoryMapper;
  NewsMapper newsMapper;
  PublicationMapper publicationMapper;

  @PostConstruct
  public void register() {
    registry.register("category", DataLoader.newDataLoader(new BatchLoader<String, CategoryDto>() {
      @Override
      public CompletionStage<List<CategoryDto>> load(final List<String> keys) {
        return CompletableFuture.supplyAsync(() -> {
          List<Integer> ids = keys.stream().map(key -> {
            try {
              if (key != null) {
                return new NodeId<Integer>(key).getKey(Integer.class);
              }
            } catch (Exception e) {
              e.printStackTrace();
            }

            return null;
          }).collect(Collectors.toList());

          // fetch from database
          List<Category> data = categoryMapper.findByIds(ids);
          List<CategoryDto> res = ids.stream().map(id -> {
            for (Category item : data) {
              if (item.getId().equals(id)) {
                return new CategoryDto(item);
              }
            }

            return null;
          }).collect(Collectors.toList());

          return res;
        });
      }
    }));

    registry.register("news", DataLoader.newDataLoader(new BatchLoader<String, NewsDto>() {
      @Override
      public CompletionStage<List<NewsDto>> load(final List<String> keys) {
        return CompletableFuture.supplyAsync(() -> {
          List<Long> ids = keys.stream().map(key -> {
            try {
              if (key != null) {
                return new NodeId<Long>(key).getKey(Long.class);
              }
            } catch (Exception e) {
              e.printStackTrace();
            }

            return null;
          }).collect(Collectors.toList());

          // fetch from database
          List<News> data = newsMapper.findByIds(ids);

          return ids.stream().map(id -> {
            for (News item : data) {
              if (item.getId().equals(id)) {
                return new NewsDto(item);
              }
            }

            return null;
          }).collect(Collectors.toList());
        });
      }
    }));

    registry.register("publication", DataLoader.newDataLoader(new BatchLoader<String, PublicationDto>() {
      @Override
      public CompletionStage<List<PublicationDto>> load(final List<String> keys) {
        return CompletableFuture.supplyAsync(() -> {
          List<Long> ids = keys.stream().map(key -> {
            try {
              if (key != null) {
                return new NodeId<Long>(key).getKey(Long.class);
              }
            } catch (Exception e) {
              e.printStackTrace();
            }

            return null;
          }).collect(Collectors.toList());

          // fetch from database
          List<Publication> data = publicationMapper.findByIds(ids);

          return ids.stream().map(id -> {
            for (Publication item : data) {
              if (item.getId().equals(id)) {
                return new PublicationDto(item);
              }
            }

            return null;
          }).collect(Collectors.toList());
        });
      }
    }));
  }

  @Autowired
  public void setCategoryMapper(CategoryMapper categoryMapper) {
    this.categoryMapper = categoryMapper;
  }

  @Autowired
  public void setNewsMapper(NewsMapper newsMapper) {
    this.newsMapper = newsMapper;
  }

  @Autowired
  public void setPublicationMapper(PublicationMapper publicationMapper) {
    this.publicationMapper = publicationMapper;
  }
}
