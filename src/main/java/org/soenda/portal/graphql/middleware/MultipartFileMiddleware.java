package org.soenda.portal.graphql.middleware;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.soenda.portal.data.FileData;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import graphql.GraphQLContext;

public class MultipartFileMiddleware implements IMiddleware {
  Map<String, Object> variables;

  public MultipartFileMiddleware(Map<String, Object> variables) {
    this.variables = variables;
  }

  @Override
  public boolean handle(GraphQLContext.Builder context, HttpServletRequest req) {
    MultipartHttpServletRequest multipartReq = (MultipartHttpServletRequest) req;

    for (Map.Entry<String, List<MultipartFile>> item : multipartReq.getMultiFileMap().entrySet()) {
      List<MultipartFile> files = item.getValue();

      if (files.size() > 0) {
        String[] keys = item.getKey().replaceFirst("^files?:", "").split("\\.");

        try {
          Map<String, Object> tmp = null;

          for (String key : keys) {
            Object variable = variables.get(key);

            if (variable instanceof Map) {
              tmp = (Map<String, Object>) variable;
            } else {
              // multiple files
              if (tmp instanceof List) {
                List<FileData> uploadFiles = new ArrayList<>();

                for (MultipartFile file : files) {
                  uploadFiles.add(new FileData(file));
                }

                tmp.put(key, uploadFiles);
              }
              // single file
              else {
                tmp.put(key, new FileData(files.get(0)));
              }
            }
          }

        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    return true;
  }
}
