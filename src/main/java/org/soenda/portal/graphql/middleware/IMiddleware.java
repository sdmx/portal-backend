package org.soenda.portal.graphql.middleware;

import javax.servlet.http.HttpServletRequest;
import graphql.GraphQLContext;

public interface IMiddleware {
  public boolean handle(GraphQLContext.Builder context, HttpServletRequest req) throws Exception;
}
