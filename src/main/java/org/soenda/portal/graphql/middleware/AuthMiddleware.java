package org.soenda.portal.graphql.middleware;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.joda.time.DateTime;
import org.soenda.portal.data.db.User;
import org.soenda.portal.data.db.mapper.UserMapper;
import org.soenda.portal.service.auth.TokenManager;
import graphql.GraphQLContext;
import graphql.GraphqlErrorException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

public class AuthMiddleware implements IMiddleware {
  TokenManager tokenManager;
  UserMapper userMapper;

  public AuthMiddleware(TokenManager tokenManager, UserMapper userMapper) {
    this.tokenManager = tokenManager;
    this.userMapper = userMapper;
  }

  @Override
  public boolean handle(GraphQLContext.Builder context, HttpServletRequest req) throws Exception {
    String authorization = req.getHeader("Authorization");

    if (authorization != null) {
      String token = authorization.replaceFirst("\\s*Bearer\\s*", "");
      Jws<Claims> claims = tokenManager.parse(token);

      verifyToken(claims);
      addUserToContext(context, claims);
    }

    return true;
  }

  private void verifyToken(Jws<Claims> claims) {
    DateTime exp = new DateTime(claims.getBody().get("exp"));

    // token has expired
    if (exp.isBeforeNow()) {
      throw new GraphqlErrorException.Builder().message("Token has expired!").build();
    }
  }

  private void addUserToContext(GraphQLContext.Builder context, Jws<Claims> claims) {
    Integer id = (Integer) claims.getBody().get("sub");
    Optional<User> user = userMapper.findById(id);

    if (!user.isPresent()) {
      throw new GraphqlErrorException.Builder()
        .message("User is unknown!")
        .build();
    }

    context.of("user", user.get());
  }
}
