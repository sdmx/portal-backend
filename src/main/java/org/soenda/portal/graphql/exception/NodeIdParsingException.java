package org.soenda.portal.graphql.exception;

import lombok.Getter;

public class NodeIdParsingException extends Exception {
  private static final long serialVersionUID = 1L;

  @Getter
  String message;

  public NodeIdParsingException(Exception exception) {
    message = exception.getMessage();
  }
}
