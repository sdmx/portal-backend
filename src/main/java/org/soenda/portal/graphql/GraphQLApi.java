package org.soenda.portal.graphql;

import java.util.Map;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.soenda.portal.data.db.mapper.UserMapper;
import org.soenda.portal.graphql.middleware.AuthMiddleware;
import org.soenda.portal.graphql.middleware.MultipartFileMiddleware;
import org.soenda.portal.service.auth.TokenManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.GraphQLContext;

@CrossOrigin
@RestController
@RequestMapping("graphql")
class GraphQLApi {
  private final static Logger log = Logger.getLogger(GraphQLApi.class.getName());

  Environment env;
  GraphQL graphQL;
  GraphQLDataLoader dataLoader;
  ObjectMapper objectMapper;
  TokenManager tokenManager;
  UserMapper userMapper;

  @Autowired
  public GraphQLApi(Environment env, GraphQL graphQL, ObjectMapper objectMapper) {
    this.env = env;
    this.graphQL = graphQL;
    this.objectMapper = objectMapper;
  }

  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE,
          MediaType.APPLICATION_FORM_URLENCODED_VALUE, "application/graphql"})
  public Map<String, Object> index(HttpServletRequest req) throws Exception {
    GraphQLContext.Builder context = GraphQLContext.newContext();
    ExecutionInput.Builder input =
        ExecutionInput.newExecutionInput().dataLoaderRegistry(dataLoader.getRegistry());

    new AuthMiddleware(tokenManager, userMapper).handle(context, req);

    switch (req.getContentType()) {
      case "application/graphql":
      case MediaType.APPLICATION_JSON_VALUE:
        try {
          GraphQLQuery query = objectMapper.readValue(req.getInputStream(), GraphQLQuery.class);
          input.query(query.getQuery());

          if (query.hasVariables()) {
            input.variables(query.getVariables());
          }

          context.of("request", req);
        } catch (Exception e) {
          e.printStackTrace();
        }
        break;

      default:
      case MediaType.APPLICATION_FORM_URLENCODED_VALUE:
      case MediaType.MULTIPART_FORM_DATA_VALUE:
        TypeReference<Map<String, Object>> typeRef = new TypeReference<Map<String, Object>>() {
        };
        Map<String, Object> variables =
            objectMapper.readValue(req.getParameter("variables"), typeRef);

        new MultipartFileMiddleware(variables).handle(context, req);

        if (variables.size() > 0) {
          input.variables(variables);
        }

        input.query(req.getParameter("query"));
        context.of("request", (MultipartHttpServletRequest) req);
        break;
    }

    ExecutionResult result = graphQL.execute(input.context(context.build()).build());

    // log errors
    result.getErrors().stream().forEach(e -> {
      log.warning(e.toString());
    });

    return result.toSpecification();
  }

  @GetMapping
  public String documentation() {
    return "GraphQL Endpoint";
  }

  @PostMapping(value = "test", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
  public ResponseEntity<Object> test(HttpServletRequest req,
      @RequestParam(value = "thumbnail", required = false) MultipartFile files) {
    System.out.println(files);

    try {
      System.out.println(req.getParts());
    } catch (Exception e) {
      e.printStackTrace();
    }

    return new ResponseEntity<>(req.getParameter("query"), HttpStatus.OK);
  }

  @Autowired
  public void setDataLoader(GraphQLDataLoader dataLoader) {
    this.dataLoader = dataLoader;
  }

  @Autowired
  public void setUserMapper(UserMapper userMapper) {
    this.userMapper = userMapper;
  }

  @Autowired
  public void setTokenManager(TokenManager tokenManager) {
    this.tokenManager = tokenManager;
  }
}
