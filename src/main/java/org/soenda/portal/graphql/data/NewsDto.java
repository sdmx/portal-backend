package org.soenda.portal.graphql.data;

import java.util.List;
import java.util.stream.Collectors;
import org.joda.time.DateTime;
import org.soenda.portal.data.FileData;
import org.soenda.portal.data.db.News;
import org.soenda.portal.data.db.enums.Privilege;
import org.soenda.portal.data.db.enums.Status;
import org.soenda.portal.graphql.type.INode;
import org.soenda.portal.graphql.type.NodeId;
import lombok.Data;

@Data
public class NewsDto implements INode<News> {
  private static final long serialVersionUID = 1L;

  News node;
  String name;
  String body;
  List<String> links;
  FileData thumbnail;
  String status;
  String privilege;
  Boolean isPublished;
  DateTime created;
  DateTime updated;

  public NewsDto(News node) {
    this.node = node;
    name = node.getName();
    body = node.getBody();
    // thumbnail = node.getThumbnail() != null ? node.getThumbnail() : null;
    thumbnail = node.getThumbnail();
    links = node.getLinks();
    status = Status.get(node.getStatus()).name();
    privilege = Privilege.get(node.getPrivilege()).name();
    isPublished = node.getIsPublished();
    created = node.getCreated();
    updated = node.getUpdated();
  }

  @Override
  public String getId() {
    return NodeId.generate("news", node.getId());
  }

  public static List<NewsDto> transform(List<News> data) {
    if (data != null && data.size() > 0) {
      return data.stream().map(item -> new NewsDto(item)).collect(Collectors.toList());
    }

    return null;
  }
}
