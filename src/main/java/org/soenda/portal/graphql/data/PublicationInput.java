package org.soenda.portal.graphql.data;

import java.util.List;
import java.util.stream.Collectors;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.soenda.portal.data.FileData;
import org.soenda.portal.data.db.Publication;
import org.soenda.portal.graphql.type.NodeId;
import graphql.schema.DataFetchingEnvironment;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PublicationInput implements IMutationInput<Publication> {
  String name;
  String body;
  List<String> links;
  List<String> categories;
  FileData thumbnail;

  public static PublicationInput parse(ObjectMapper objectMapper, DataFetchingEnvironment env) {
    return objectMapper.convertValue(env.getArgument("input"), PublicationInput.class);
  }

  @Override
  public Publication getData() {
    final Publication data = new Publication();
    data.setName(name);
    data.setBody(body);

    if (links != null && links.size() > 0) {
      data.setLinks(links);
    }

    if (categories != null && categories.size() > 0) {
      data.setCategoryIds(categories.stream().map(nodeId -> {
        return NodeId.forceParse(nodeId, Integer.class);
      }).collect(Collectors.toList()));
    }

    if (thumbnail != null) {
      data.setThumbnail(thumbnail);
    }

    return data;
  }
}
