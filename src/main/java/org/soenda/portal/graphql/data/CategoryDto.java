package org.soenda.portal.graphql.data;

import java.util.List;
import java.util.stream.Collectors;
import org.soenda.portal.data.db.Category;
import org.soenda.portal.graphql.type.INode;
import org.soenda.portal.graphql.type.NodeId;
import lombok.Data;

@Data
public class CategoryDto implements INode<Category> {
  private static final long serialVersionUID = 1L;

  Category node;
  String name;

  public CategoryDto(Category node) {
    this.node = node;
    name = node.getName();
  }

  @Override
  public String getId() {
    return NodeId.generate("category", node.getId());
  }

  public static List<CategoryDto> transform(List<Category> data) {
    if (data != null && data.size() > 0) {
      return data.stream().map(item -> new CategoryDto(item)).collect(Collectors.toList());
    }

    return null;
  }
}
