package org.soenda.portal.graphql.data;

public interface IMutationInput<T> {
  public abstract T getData();
}
