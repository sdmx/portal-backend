package org.soenda.portal.graphql.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.soenda.portal.data.db.Category;
import org.soenda.portal.graphql.type.NodeId;
import graphql.schema.DataFetchingEnvironment;
import lombok.Data;

@Data
public class CategoryInput implements IMutationInput<Category> {
  String name;
  String parentId;

  public static CategoryInput parse(ObjectMapper objectMapper, DataFetchingEnvironment env) {
    return objectMapper.convertValue(env.getArgument("input"), CategoryInput.class);
  }

  @Override
  public Category getData() {
    Category data = new Category();
    data.setName(name);

    if (parentId != null) {
      data.setParentId(NodeId.forceParse(parentId, Integer.class));
    }

    return data;
  }
}
