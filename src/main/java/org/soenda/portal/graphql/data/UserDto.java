package org.soenda.portal.graphql.data;

import java.util.List;
import java.util.stream.Collectors;
import org.soenda.portal.data.db.User;
import org.soenda.portal.data.db.enums.Status;
import org.soenda.portal.graphql.type.INode;
import org.soenda.portal.graphql.type.NodeId;
import lombok.Data;

@Data
public class UserDto implements INode<User> {
  private static final long serialVersionUID = 1L;

  User node;
  String username;
  String name;
  String email;
  String status;
  // Integer organisationId;
  // Integer positionId;

  public UserDto(User node) {
    this.node = node;
    username = node.getUsername();
    name = node.getName();
    email = node.getEmail();
    status = Status.get(node.getStatus()).name();
  }

  @Override
  public String getId() {
    return NodeId.generate("user", node.getId());
  }

  public static List<UserDto> transform(List<User> data) {
    if (data != null && data.size() > 0) {
      return data.stream().map(item -> new UserDto(item)).collect(Collectors.toList());
    }

    return null;
  }
}
