package org.soenda.portal.graphql.data;

import java.util.List;
import java.util.stream.Collectors;
import org.joda.time.DateTime;
import org.soenda.portal.data.FileData;
import org.soenda.portal.data.db.Publication;
import org.soenda.portal.data.db.enums.Privilege;
import org.soenda.portal.data.db.enums.Status;
import org.soenda.portal.graphql.type.INode;
import org.soenda.portal.graphql.type.NodeId;
import lombok.Data;

@Data
public class PublicationDto implements INode<Publication> {
  private static final long serialVersionUID = 1L;

  Publication node;
  String name;
  String body;
  List<String> links;
  FileData thumbnail;
  String status;
  String privilege;
  Boolean isPublished;
  DateTime created;
  DateTime updated;

  public PublicationDto(Publication node) {
    this.node = node;
    name = node.getName();
    body = node.getBody();
    // thumbnail = node.getThumbnail() != null ? node.getThumbnail() : null;
    thumbnail = node.getThumbnail();
    links = node.getLinks();
    status = Status.get(node.getStatus()).name();
    privilege = Privilege.get(node.getPrivilege()).name();
    isPublished = node.getIsPublished();
    created = node.getCreated();
    updated = node.getUpdated();
  }

  @Override
  public String getId() {
    return NodeId.generate("publication", node.getId());
  }

  public static List<PublicationDto> transform(List<Publication> data) {
    if (data != null && data.size() > 0) {
      return data.stream().map(item -> new PublicationDto(item)).collect(Collectors.toList());
    }

    return null;
  }
}
