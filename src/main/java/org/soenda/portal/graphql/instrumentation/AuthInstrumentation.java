package org.soenda.portal.graphql.instrumentation;

import org.soenda.portal.data.db.User;
import graphql.GraphQLContext;
import graphql.execution.ExecutionContext;
import graphql.execution.ExecutionPath;
import graphql.execution.instrumentation.SimpleInstrumentation;
import graphql.execution.instrumentation.parameters.InstrumentationFieldFetchParameters;
import graphql.schema.DataFetcher;

public class AuthInstrumentation extends SimpleInstrumentation {
  private boolean isAuthorized(ExecutionContext executionContext, String... scopes) {
    // GraphQLContext ctx = executionContext.getContext();
    // User user = (User) ctx.get("user");

    // TODO: check permission access
    // if (false) {
    // GraphQLError error = GraphqlErrorBuilder.newError().message("Unauthorized Access!").build();
    // executionContext.addError(error);
    // }

    return true;
  }

  private boolean isAuthenticated(ExecutionContext executionContext) {
    GraphQLContext ctx = executionContext.getContext();
    return ctx.hasKey("user");
  }

  @Override
  public DataFetcher<?> instrumentDataFetcher(final DataFetcher<?> dataFetcher,
      final InstrumentationFieldFetchParameters parameters) {
    final ExecutionPath executionPath = parameters.getExecutionStepInfo().getPath();
    final ExecutionContext ctx = parameters.getExecutionContext();

    boolean hasAccess = true;

    switch (executionPath.getSegmentName()) {
      case "createCategory":
        hasAccess = isAuthorized(ctx, "category.create");
      case "updateCategory":
        hasAccess = isAuthorized(ctx, "category.update");
      case "deleteCategory":
        hasAccess = isAuthorized(ctx, "category.delete");

      case "createNews":
        hasAccess = isAuthorized(ctx, "news.create");
      case "updateNews":
        hasAccess = isAuthorized(ctx, "news.update");
      case "deleteNews":
        hasAccess = isAuthorized(ctx, "news.delete");
    }

    if (!hasAccess) {
      return null;
    }

    return dataFetcher;
  }
}
