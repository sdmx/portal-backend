package org.soenda.portal.graphql.type;

import java.util.List;
import graphql.relay.DefaultConnection;
import graphql.relay.Edge;
import graphql.relay.PageInfo;
import lombok.Getter;
import lombok.Setter;

public class ConnectionImpl<T> extends DefaultConnection<T> {
  @Getter @Setter
  Long totalCount;

  public ConnectionImpl(List<Edge<T>> edges, PageInfo pageInfo) {
    super(edges, pageInfo);
  }

  public ConnectionImpl(List<Edge<T>> edges, PageInfo pageInfo, Long totalCount) {
    super(edges, pageInfo);
    this.totalCount = totalCount;
  }
}
