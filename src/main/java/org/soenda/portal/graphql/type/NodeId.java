package org.soenda.portal.graphql.type;

import java.util.Base64;
import org.soenda.portal.graphql.exception.NodeIdParsingException;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NodeId<T> {
  String id;
  String entity;
  Object key;

  public NodeId(String id) throws NodeIdParsingException {
    if (id != null) {
      this.id = id;

      try {
        String content = new String(Base64.getDecoder().decode(id.getBytes()));
        parse(content);
      } catch (final Exception e) {
        throw new NodeIdParsingException(e);
      }
    }
  }

  private void parse(String content) {
    String[] param = content.split(":");
    entity = param[0];
    key = param[1];
  }

  public static String generate(String entity, Object key) {
    String content = String.format("%s:%s", entity, key.toString());
    return new String(Base64.getEncoder().encode(content.getBytes()));
  }

  public static <T> T forceParse(String nodeId, Class<T> type) {
    try {
      final NodeId<T> id = new NodeId<T>(nodeId);
      return id.getKey(type);
    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  public T getKey(Class<T> type) throws Exception {
    return key == null ? null : type.getConstructor(String.class).newInstance(key.toString());
  }

  public Boolean isPresent() {
    return id != null;
  }
}
