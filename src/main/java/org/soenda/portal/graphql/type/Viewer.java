package org.soenda.portal.graphql.type;

import org.soenda.portal.data.db.User;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Viewer {
  String id;
  User node;

  public Viewer(User user) {
    if (user != null) {
      id = NodeId.generate("user", user.getId());
      node = user;
    }
    else {
      id = NodeId.generate("user", "guest");
    }
  }
}
