package org.soenda.portal.graphql.type;

import java.io.Serializable;

public interface INode<T> extends Serializable {
  public String getId();
  public T getNode();
}
