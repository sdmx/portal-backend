/**
 * Authentication & Authorization
 */

/** Organisations & Positions */
create table organisations (
  id int auto_increment primary key,
  parent_id int references organisations(id) on delete cascade,
  name varchar(255) not null,
  type char(1) not null
);

create table positions (
  id int auto_increment primary key,
  parent_id int references positions(id) on delete cascade,
  name varchar(255) not null,
  has_organisation char(1) default '1'
);

/** User accounts */
create table users (
  id int auto_increment primary key,
  username varchar(100) not null unique,
  name varchar(255) not null,
  email varchar(100),
  organisation_id int references organisations(id) on delete cascade,
  position_id int references positions(id) on delete cascade,
  deleted timestamp,
  status char(1) default '1'
);

create table password_resets (
  user_id int not null references users(id) on delete cascade,
  access_key varchar(100) not null,
  created timestamp default CURRENT_TIMESTAMP,
  expired timestamp not null
);

/** Roles & Permissions */
CREATE TABLE roles (
  id int auto_increment primary key,
  name varchar(100) not null unique,
  description varchar(255)
);

CREATE TABLE permissions (
  id int auto_increment primary key,
  name varchar(100) not null unique,
  description varchar(255)
);

CREATE TABLE users_roles (
  user_id int not null references users(id) on delete cascade,
  role_id int not null references roles(id) on delete cascade
);

CREATE TABLE roles_permissions (
  role_id int not null references roles(id) on delete cascade,
  permission_id int not null references permissions(id) on delete cascade
);

/**
 * Content management
 */

create table categories (
  id int auto_increment primary key,
  name varchar(255) not null,
  parent_id int references categories(id) on delete cascade
);

/** News */
create table news (
  id int auto_increment primary key,
  name varchar(255) not null,
  body text,
  links text,
  thumbnail varchar(255),
  user_id int references users(id),
  attributes text,
  status char(1) default '0',
  privilege char(1) default '1',
  is_published char(1) default '0',
  created timestamp default CURRENT_TIMESTAMP,
  updated timestamp default CURRENT_TIMESTAMP
);

create table news_categories (
  news_id int not null references news(id) on delete cascade,
  category_id int not null references categories(id) on delete cascade
);

create table news_approvals (
  id int auto_increment primary key,
  user_id int not null references users(id) on delete cascade,
  news_id int not null references news(id) on delete cascade,
  status char(1) default '0',
  created timestamp default CURRENT_TIMESTAMP,
  updated timestamp default CURRENT_TIMESTAMP
);

create table news_approval_manifests (
  id int auto_increment primary key,
  news_approval_id int not null references news_approvals(id) on delete cascade,
  user_id int not null references users(id) on delete cascade,
  status char(1) default '0',
  is_edited char(1) default '0',
  is_mandatory char(1) default '0',
  notes text,
  order_num smallint not null,
  created timestamp default CURRENT_TIMESTAMP,
  updated timestamp
);

/** Publications */
create table publications (
  id int auto_increment primary key,
  name varchar(255) not null,
  body text not null,
  links text,
  thumbnail text,
  user_id int references users(id) on delete cascade,
  attributes text,
  status char(1) default '0',
  privilege char(1) default '1',
  is_published char(1) default '0',
  created timestamp default CURRENT_TIMESTAMP,
  updated timestamp default CURRENT_TIMESTAMP
);

create table publication_attachments (
  id int auto_increment not null primary key,
  publication_id int not null references publications(id) on delete cascade,
  display_name varchar(255) not null,
  filepath varchar(255) not null,
  created timestamp default CURRENT_TIMESTAMP
);

create table publications_categories (
  publication_id int not null references publications(id) on delete cascade,
  category_id int not null references categories(id) on delete cascade
);

create table publication_approvals (
  id int auto_increment primary key,
  user_id int not null references users(id) on delete cascade,
  publication_id int not null references publications(id) on delete cascade,
  status char(1) default '0',
  created timestamp default CURRENT_TIMESTAMP,
  updated timestamp default CURRENT_TIMESTAMP
);

create table publication_approval_manifests (
  id int auto_increment primary key,
  publication_approval_id int not null references publication_approvals(id) on delete cascade,
  user_id int not null references users(id) on delete cascade,
  status char(1) default '0',
  is_edited char(1) default '0',
  is_mandatory char(1) default '0',
  notes text,
  order_num smallint not null,
  created timestamp default CURRENT_TIMESTAMP,
  updated timestamp
);

/** Approval configurations */
create table approval_configs (
  id int auto_increment primary key,
  user_id int not null references users(id) on delete cascade,
  content_type char(1) not null,
  org_id int not null references organisations(id) on delete cascade,
  pos_id int not null references positions(id) on delete cascade,
  created timestamp default CURRENT_TIMESTAMP,
  updated timestamp default CURRENT_TIMESTAMP
);

create table approval_config_details (
  id int auto_increment primary key,
  approval_config_id int not null references approval_configs(id) on delete cascade,
  org_id int not null references organisations(id) on delete cascade,
  pos_id int not null references positions(id) on delete cascade,
  order_num smallint not null,
  is_mandatory char(1) default '1'
);
